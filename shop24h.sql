-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 26, 2021 at 07:44 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shop24h`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `rate_star` decimal(19,2) DEFAULT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `comments` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `name`, `rate_star`, `product_id`, `user_id`, `comments`) VALUES
(6, 'Dang Van Huy', '4.00', 10, 2, 'test cmt giay thu 2'),
(7, 'Dang Van Huy', '4.00', 6, 2, 'mot cai comment ra chi la tuyet voi'),
(8, 'Dang Van Huy', '5.00', 6, 2, 'them mot cai comment kha nua ne'),
(20, 'Dang Van Huy', '3.00', 8, 2, 'tét nha'),
(26, 'HuyDV', '5.00', 10, 2, 'test lai lan cuoi'),
(30, 'NhuMay', NULL, 10, 4, 'cuoi cung cung xong phan history va danh gia san pham'),
(31, 'NhuMay', NULL, 12, 4, 'hang rat chat luong'),
(32, 'NhuMay', '5.00', 12, 4, 'hang rat chat luong'),
(33, 'HuyDV', '4.00', 27, 2, 'giay gi trong nu tinh qua'),
(34, 'BaQuat', NULL, 6, 6, 'ddd'),
(35, 'BaQuat', '5.00', 7, 6, 'test comment'),
(36, 'BaQuat', '1.00', 7, 6, 'test diem danh gia trung binh');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` bigint(20) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `credit_limit` int(11) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `postal_code` varchar(255) DEFAULT NULL,
  `sales_rep_employee_number` int(11) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `address`, `city`, `country`, `credit_limit`, `first_name`, `last_name`, `phone_number`, `postal_code`, `sales_rep_employee_number`, `state`, `user_id`) VALUES
(38, 'Ha Noi', 'Ha Noi', 'Viet Nam', 100, 'Dang', 'Van Huy', '987654321', '100000', 10, 'None', 2),
(48, 'Hoang Mai', 'Da Nang', 'Viet Nam', 1000, 'Nhu', 'May', '9876545678', '100000', 12, 'None', 4),
(56, '69 truong son', 'Ho Chi Minh', 'Viet Nam', 5000, 'Cao Ba', 'Quat', '0901674116', '100000', 13, 'None', 6);

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` bigint(20) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `extension` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `job_title` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `office_code` int(11) DEFAULT NULL,
  `report_to` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(97);

-- --------------------------------------------------------

--
-- Table structure for table `offices`
--

CREATE TABLE `offices` (
  `id` bigint(20) NOT NULL,
  `address_line` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `territory` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL,
  `comments` varchar(255) DEFAULT NULL,
  `order_date` date DEFAULT NULL,
  `required_date` date DEFAULT NULL,
  `shipped_date` date DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `customer_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `comments`, `order_date`, `required_date`, `shipped_date`, `status`, `customer_id`) VALUES
(76, '', '2021-09-25', '2021-09-25', NULL, 'request', 56),
(80, '', '2021-09-25', '2021-09-25', NULL, 'request', 38),
(84, '', '2021-09-25', '2021-09-25', NULL, 'request', 48),
(88, '', '2021-09-25', '2021-09-25', NULL, 'request', 48),
(92, '', '2021-09-26', '2021-09-26', NULL, 'request', 48);

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `id` bigint(20) NOT NULL,
  `price_each` decimal(19,2) DEFAULT NULL,
  `quantity_order` int(11) DEFAULT NULL,
  `order_id` bigint(20) DEFAULT NULL,
  `product_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`id`, `price_each`, `quantity_order`, `order_id`, `product_id`) VALUES
(77, '250.00', 1, 76, 6),
(78, '200.00', 2, 76, 7),
(81, '175.00', 3, 80, 11),
(82, '350.00', 2, 80, 27),
(85, '200.00', 1, 84, 7),
(86, '250.00', 3, 84, 10),
(89, '400.00', 2, 88, 14),
(90, '300.00', 1, 88, 13),
(93, '400.00', 1, 92, 14),
(94, '300.00', 1, 92, 13),
(95, '200.00', 1, 92, 12);

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` bigint(20) NOT NULL,
  `ammount` decimal(19,2) DEFAULT NULL,
  `check_number` varchar(255) DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL,
  `customer_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `ammount`, `check_number`, `payment_date`, `customer_id`) VALUES
(79, '650.00', '', '2021-09-25 00:00:00', 56),
(83, '1225.00', '', '2021-09-25 00:00:00', 38),
(87, '950.00', '', '2021-09-25 00:00:00', 48),
(91, '1100.00', '', '2021-09-25 00:00:00', 48),
(96, '900.00', '', '2021-09-26 00:00:00', 48);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) NOT NULL,
  `product_code` varchar(255) DEFAULT NULL,
  `product_line_id` bigint(20) DEFAULT NULL,
  `buy_price` decimal(19,2) DEFAULT NULL,
  `product_description` varchar(1000) DEFAULT NULL,
  `product_image` varchar(255) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `quantity_in_stock` int(11) DEFAULT NULL,
  `sell_price` decimal(19,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `product_code`, `product_line_id`, `buy_price`, `product_description`, `product_image`, `product_name`, `quantity_in_stock`, `sell_price`) VALUES
(6, 'NA001', 1, '200.00', 'ĐÔI GIÀY MŨI VỎ SÒ CLASSIC VỚI THIẾT KẾ HIỆN ĐẠI. Đôi khi cần phải khuấy động phong cách. Mang đến nét mới mẻ cho outfit của bạn với đôi giày adidas Superstar này. Pha trộn các chi tiết cổ điển cùng các điểm nhấn sắc màu hiện đại, đôi giày trainer bằng da thanh thoát này trung thành với di sản bóng rổ mà vẫn chuẩn trend đầy táo bạo. Đế cupsole bằng cao su và lót giày đúc mang đến cảm giác thoải mái bất kể bạn đang xuống phố khoe outfit chặt chém hay chỉ đơn giản là tụ tập cùng bạn bè.  Sản phẩm này có sử dụng chất liệu tái chế, là một phần cam kết của chúng tôi hướng tới chấm dứt rác thải nhựa. 20% thân giày làm từ chất liệu có chứa tối thiểu 50% thành phần tái chế.', 'https://assets.adidas.com/images/h_840,f_auto,q_auto:sensitive,fl_lossy,c_fill,g_auto/c20c41c249044b27bcd5ad2701727449_9366/Giay_Superstar_trang_H05250_01_standard.jpg', 'GIÀY SUPERSTAR', 100, '250.00'),
(7, 'NA002', 1, '150.00', 'NĂNG LƯỢNG CHỈ LÀ NĂNG LƯỢNG CHO ĐẾN KHI VÀO TAY ULTRABOOST 21. Hàng loạt bản mẫu. Hàng loạt cải tiến. Hàng loạt thử nghiệm. Đồng hành cùng chúng tôi trên hành trình tìm kiếm sự hòa hợp đỉnh cao giữa trọng lượng, sự êm ái và độ đàn hồi. Ultraboost 21. Đón chào nguồn năng lượng hoàn trả phi thường.', 'https://assets.adidas.com/images/h_840,f_auto,q_auto:sensitive,fl_lossy,c_fill,g_auto/aee516268d3c4f73a3bfab97004bd862_9366/SUPERSTAR_DJen_FY1589_01_standard.jpg', 'SUPERSTAR BLACK', 100, '200.00'),
(8, 'NA003', 1, '250.00', 'NÂNG TẦM BUỔI CHẠY CỦA BẠN VỚI ĐÔI GIÀY ULTRABOOST 21 NÀY. Nỗ lực vì chính bản thân bạn. Đôi giày chạy bộ adidas Ultraboost 21 này giúp mọi chuyện dễ dàng hơn. Siêu nhẹ nhưng vẫn giữ nguyên độ nâng đỡ, thân giày adidas Primeknit+ thích ứng theo hình dáng bàn chân khi chuyển động. Lớp đệm Boost đàn hồi cho cảm giác trợ lực như lời nhắc bạn luôn có thể cố gắng thêm một sải bước, một dãy phố hay thậm chí hẳn một dặm. (Nhớ thêm vài bài hát vào playlist chạy bộ. Biết đâu bạn sẽ cần đến.)', 'https://assets.adidas.com/images/h_840,f_auto,q_auto:sensitive,fl_lossy,c_fill,g_auto/8453a9e0695644ae9002ad2100ad29c9_9366/Giay_UltraBoost_21_DJen_S23870_01_standard.jpg', 'GIÀY ULTRABOOST 21', 100, '300.00'),
(9, 'NA004', 1, '100.00', 'ĐÔI GIÀY TRAINER SIÊU THOẢI MÁI THỂ HIỆN TINH THẦN PRIDE ĐẦY MÀU SẮC TRONG BẠN. Pride không chỉ gói gọn trong một tháng hay một lễ hội, mà quan trọng là thể hiện con người thật trong bạn. Mỗi ngày. Nằm trong bộ sưu tập Pride, đôi giày adidas này đón nhận và tôn vinh tình yêu thương dưới mọi hình thức biểu đạt đẹp đẽ nhất. Sắc màu rực rỡ, táo bạo nổi bật trên 3 Sọc và gót giày.', 'https://assets.adidas.com/images/h_840,f_auto,q_auto:sensitive,fl_lossy,c_fill,g_auto/88141cb3d059464eb362acbd01210602_9366/Giay_ZX_1K_Boost_Pride_trang_GW2418_01_standard.jpg', 'GIÀY ZX 1K BOOST PRIDE', 100, '150.00'),
(10, 'NA005', 1, '200.00', 'NĂNG LƯỢNG CHỈ THUẦN LÀ NĂNG LƯỢNG CHO ĐẾN KHI VÀO TAY ULTRABOOST 21. Làm sao để cải tiến một đôi giày hoàn trả năng lượng? Hãy tăng cường hơn nữa khả năng hoàn trả năng lượng. Đôi giày adidas Ultraboost 21 này cho cảm giác siêu thoải mái, giúp bạn nhẹ nhàng tăng cự ly và cải thiện split time. Bên cạnh đế giữa được bổ sung lượng Boost, giày còn có hệ thống Linear Energy Push tiếp thêm năng lượng cho từng bước chân.  Sản phẩm này may bằng vải công nghệ Primeblue, chất liệu tái chế hiệu năng cao có sử dụng sợi Parley Ocean Plastic. 50% thân giày làm bằng vải dệt, 75% vải dệt bằng sợi Primeblue. Không sử dụng polyester nguyên sinh.', 'https://assets.adidas.com/images/h_840,f_auto,q_auto:sensitive,fl_lossy,c_fill,g_auto/58d3ebdf9dd94e7c80c9ad0b01060339_9366/Giay_Chay_Bo_UltraBoost_21_Tokyo_trang_S23863_01_standard.jpg', 'GIÀY CHẠY BỘ ULTRABOOST 21 TOKYO', 100, '250.00'),
(11, 'NA006', 1, '125.00', 'ĐÔI GIÀY ADIDAS SUPERSTAR TRONG DIỆN MẠO MỚI MẺ. Tôn vinh một biểu tượng, theo cách ngập tràn màu sắc. Là phong cách rất được yêu thích kể từ khi xuất hiện lần đầu trên sân bóng rổ vào thập niên 1970, đôi giày adidas Superstar chưa bao giờ chấp nhận dậm chân tại chỗ, luôn tìm cách vượt qua rào cản và phá vỡ ranh giới. Phiên bản này giữ trọn tinh thần nổi loạn ấy với thiết kế đầy màu sắc và thân giày hiện đại làm từ chất liệu tổng hợp. Đế cupsole và mũi vỏ sò bằng cao su giữ nguyên cội nguồn thể thao bất kể bạn mang giày tới đâu.', 'https://assets.adidas.com/images/h_840,f_auto,q_auto:sensitive,fl_lossy,c_fill,g_auto/95f3b6936ed7415c8672ad0800cbc532_9366/Giay_Superstar_trang_H00182_01_standard.jpg', 'GIÀY SUPERSTAR', 100, '175.00'),
(12, 'NA007', 1, '125.00', 'NĂNG LƯỢNG CHỈ LÀ NĂNG LƯỢNG CHO ĐẾN KHI VÀO TAY ULTRABOOST 21. Hàng loạt bản mẫu. Hàng loạt cải tiến. Hàng loạt thử nghiệm. Đồng hành cùng chúng tôi trên hành trình tìm kiếm sự hòa hợp đỉnh cao giữa trọng lượng, sự êm ái và độ đàn hồi. Ultraboost 21. Đón chào nguồn năng lượng hoàn trả phi thường.', 'https://assets.adidas.com/images/h_840,f_auto,q_auto:sensitive,fl_lossy,c_fill,g_auto/8ff66664f3e942fb84dcac8b0125e997_9366/Giay_UltraBoost_21_Xam_FY0381_01_standard.jpg', 'GIÀY ULTRABOOST 21', 100, '200.00'),
(13, 'NA008', 1, '200.00', 'GIÀY CHẠY BỘ DÀNH CHO NGÀY NÓNG HỢP TÁC THIẾT KẾ VỚI TAKAHASHI HIROKO. Bạn đang nhễ nhại mồ hôi và còn phải leo dốc thêm 2km nữa mới về đến nhà. Nhưng bạn có thể tìm thấy nguồn năng lượng và cảm hứng từ những vận động viên sẽ thi đấu vào mùa hè tới đây khi mang đôi giày chạy bộ adidas này. Thiết kế của đôi giày là thành quả hợp tác với nghệ sĩ đến từ Tokyo Takahashi Hiroko cùng thương hiệu HIROCOLEDGE của cô. Đế giữa đàn hồi mang họa tiết hình tròn yêu thích của nhà thiết kế bởi sự phù hợp với mọi giới tính, quốc gia và thời đại. Biến vấn đề thành giải pháp Thân giày sử dụng công nghệ Primeblue, chất liệu tái chế hiệu năng cao chứa ít nhất 50% sợi Parley Ocean Plastic.', 'https://assets.adidas.com/images/h_840,f_auto,q_auto:sensitive,fl_lossy,c_fill,g_auto/cdde9668441f43248983ab450106beef_9366/Giay_Ultraboost_SUMMER.RDY_Tokyo_trang_FX0031_01_standard.jpg', 'GIÀY ULTRABOOST SUMMER.RDY TOKYO', 100, '300.00'),
(14, 'NA009', 1, '300.00', 'NÂNG TẦM BUỔI CHẠY CỦA BẠN VỚI ĐÔI GIÀY ULTRABOOST 21 NÀY. Nỗ lực vì chính bản thân bạn. Đôi giày chạy bộ adidas Ultraboost 21 này giúp mọi chuyện dễ dàng hơn. Siêu nhẹ nhưng vẫn giữ nguyên độ nâng đỡ, thân giày adidas Primeknit+ thích ứng theo hình dáng bàn chân khi chuyển động. Lớp đệm Boost đàn hồi cho cảm giác trợ lực như lời nhắc bạn luôn có thể cố gắng thêm một sải bước, một dãy phố hay thậm chí hẳn một dặm. (Nhớ thêm vài bài hát vào playlist. Biết đâu bạn sẽ cần đến.)  Sản phẩm này may bằng vải công nghệ Primeblue, chất liệu tái chế hiệu năng cao có sử dụng sợi Parley Ocean Plastic. 50% thân giày làm bằng vải dệt, 75% vải dệt bằng sợi Primeblue. Không sử dụng polyester nguyên sinh.', 'https://assets.adidas.com/images/h_840,f_auto,q_auto:sensitive,fl_lossy,c_fill,g_auto/2528244a96904403a1b1acb8009c1e61_9366/Giay_UltraBoost_21_Xam_FZ2559_01_standard.jpg', 'GIÀY ULTRABOOST 21', 100, '400.00'),
(15, 'NA010', 1, '250.00', 'ĐÔI GIÀY ADIDAS SUPERSTAR TRONG DIỆN MẠO MỚI MẺ. Tôn vinh một biểu tượng, theo cách ngập tràn màu sắc. Là phong cách rất được yêu thích kể từ khi xuất hiện lần đầu trên sân bóng rổ vào thập niên 1970, đôi giày adidas Superstar chưa bao giờ chấp nhận dậm chân tại chỗ, luôn tìm cách vượt qua rào cản và phá vỡ ranh giới. Phiên bản này giữ trọn tinh thần nổi loạn ấy với thiết kế đầy màu sắc và thân giày hiện đại làm từ chất liệu tổng hợp. Đế cupsole và mũi vỏ sò bằng cao su giữ nguyên cội nguồn thể thao bất kể bạn mang giày tới đâu.', 'https://assets.adidas.com/images/h_840,f_auto,q_auto:sensitive,fl_lossy,c_fill,g_auto/cb3d5cf87b4b457db0fead0800cbfca0_9366/Giay_Superstar_trang_H00183_01_standard.jpg', 'GIÀY SUPERSTAR', 100, '300.00'),
(16, 'NA011', 1, '200.00', 'ĐÔI GIÀY CHẠY BỘ CHO CẢM GIÁC THOẢI MÁI MỖI NGÀY. Chinh phục ngày dài thật thoải mái và sẵn sàng cho tất cả với đôi giày chạy bộ adidas này. Thân giày bằng vải lưới thoáng khí giúp đôi chân bạn luôn khô thoáng kể cả khi trời nóng. Lớp đệm trợ lực cho cảm giác đàn hồi trong từng sải bước.  Sản phẩm này có sử dụng chất liệu tái chế, là một phần cam kết của chúng tôi hướng tới chấm dứt rác thải nhựa. 20% thân giày làm từ chất liệu có chứa tối thiểu 50% thành phần tái chế.', 'https://assets.adidas.com/images/h_840,f_auto,q_auto:sensitive,fl_lossy,c_fill,g_auto/2e0840e11a2f4b6c8407ad1f018895b1_9366/Giay_Response_Super_2.0_trang_H04563_01_standard.jpg', 'GIÀY RESPONSE SUPER 2.0', 100, '300.00'),
(17, 'NA012', 1, '125.00', 'ĐÔI GIÀY SNEAKER VỪA CASUAL VỪA THỂ THAO CHO NHỊP SỐNG NĂNG ĐỘNG. Khi mang đôi giày chạy bộ này, bạn sẵn sàng hẹn hò cafe cùng bè bạn ngay cả khi đã chạy một vòng công viên. Thân giày bằng vải lưới siêu thoáng khí mang đến cho bạn cảm giác thoải mái suốt ngày dài. Đế ngoài bằng cao su bền bỉ là bệ đỡ vững chắc cho lịch trình bận rộn của bạn.', 'https://assets.adidas.com/images/h_840,f_auto,q_auto:sensitive,fl_lossy,c_fill,g_auto/35dc72b0efff4a4f863aad1000bc42ce_9366/Giay_Run_Falcon_2.0_trang_G58098_01_standard.jpg', 'GIÀY RUN FALCON 2.0', 100, '200.00'),
(18, 'NU001', 2, '150.00', '', 'https://assets.adidas.com/images/h_840,f_auto,q_auto:sensitive,fl_lossy,c_fill,g_auto/6abed98d929146dda047ab96015d8a07_9366/SUPERSTAR_trang_FY1588_01_standard.jpg', 'SUPERSTAR', 100, '200.00'),
(19, 'NU002', 2, '150.00', 'ĐÔI GIÀY CHẠY BỘ CÓ LỚP ĐỆM TINH CHỈNH CHO CẢM GIÁC THOẢI MÁI MỖI NGÀY. Chạy bộ chính là tập trung vào hiện tại. Đôi giày chạy bộ adidas này dẫn dắt bạn nhanh chóng tới tương lai. Đế giữa adidas 4D đột phá dựa trên dữ liệu chạy bộ thu thập suốt 17 năm và công nghệ gia công kỹ thuật số, giúp tăng cường thoải mái cho từng sải bước. Thân giày adidas Primeknit đảm bảo cảm giác ôm chắc chắn, thích ứng theo bàn chân trong suốt chu kỳ của bước đi.', 'https://assets.adidas.com/images/h_840,f_auto,q_auto:sensitive,fl_lossy,c_fill,g_auto/a151246b77084be89174ad260106f868_9366/Giay_adidas_4D_Futurecraft_trang_Q46229_01_standard.jpg', 'GIÀY ADIDAS 4D FUTURECRAFT', 100, '250.00'),
(20, 'NU003', 2, '250.00', 'ĐÔI GIÀY ADIDAS SUPERSTAR THANH THOÁT ĐÓN CHÀO NHỮNG NGÀY HÈ NÓNG NỰC. Đôi giày mũi vỏ sò classic chưa bao giờ ngọt ngào đến thế. Tức khắc trở thành một biểu tượng khi ra mắt trên sân bóng rổ vào thập niên 70, dòng giày adidas Superstar nổi tiếng thế giới nay có phiên bản cập nhật thật hấp dẫn. Đôi giày này có thân giày bằng vải canvas giúp tăng cường lưu thông khí vào những ngày nóng nực cùng họa tiết thêu phủ toàn bộ trông thật ngon lành gợi nhớ tới những hương vị kem yêu thích. Bạn còn chần chừ gì nữa. Hãy mang giày và sẵn sàng cho những ngày hè rực nắng và những món ngon giải nhiệt.', 'https://assets.adidas.com/images/h_840,f_auto,q_auto:sensitive,fl_lossy,c_fill,g_auto/e54ebed3d6bd4b7193fead20001ec7df_9366/Giay_Superstar_trang_H05667_01_standard.jpg', 'GIÀY SUPERSTAR', 100, '300.00'),
(21, 'NU004', 2, '150.00', 'NĂNG LƯỢNG CHỈ THUẦN LÀ NĂNG LƯỢNG CHO ĐẾN KHI VÀO TAY ULTRABOOST 21. Làm sao để cải tiến một đôi giày hoàn trả năng lượng? Hãy tăng cường hơn nữa khả năng hoàn trả năng lượng. Đôi giày adidas Ultraboost 21 này cho cảm giác siêu thoải mái, giúp bạn nhẹ nhàng tăng cự ly và cải thiện split time. Bên cạnh đế giữa được bổ sung lượng Boost, giày còn có hệ thống Linear Energy Push tiếp thêm năng lượng cho từng bước chân.  Sản phẩm này may bằng vải công nghệ Primeblue, chất liệu tái chế hiệu năng cao có sử dụng sợi Parley Ocean Plastic. 50% thân giày làm bằng vải dệt, 75% vải dệt bằng sợi Primeblue. Không sử dụng polyester nguyên sinh.', 'https://assets.adidas.com/images/h_840,f_auto,q_auto:sensitive,fl_lossy,c_fill,g_auto/f4882d178d824972931facc4011e6941_9366/Giay_Chay_Bo_UltraBoost_21_Tokyo_trang_S23840_01_standard.jpg', 'GIÀY CHẠY BỘ ULTRABOOST 21 TOKYO', 100, '200.00'),
(22, 'NU005', 2, '100.00', 'CHẠY BỘ MỖI NGÀY NHƯ TRÊN ĐƯỜNG ĐUA. adizero Boston 10 là đôi giày chạy bộ đa năng giúp tối ưu hóa những buổi tập chạy cự ly dài hàng ngày của bạn. Dựa trên bề dày di sản và thiết kế biến hóa của dòng adizero, đôi giày adizero Boston 10 mang đến cho runner các công nghệ đột phá, bao gồm lớp mút đế giữa kết hợp công nghệ LIGHTSTRIKE PRO và LIGHTSTRIKE EVA bền chắc, các thanh năng lượng ENERGYRODS và ĐẾ NGOÀI BẰNG CAO SU CONTINENTAL™ RUBBER cho cảm giác siêu nhẹ mà bền chắc, hỗ trợ đường chạy tập luyện cự ly dài hàng ngày.', 'https://assets.adidas.com/images/h_840,f_auto,q_auto:sensitive,fl_lossy,c_fill,g_auto/b911f86aa8a34332aa7bad240137368b_9366/Giay_Adizero_Boston_10_mau_xanh_la_FZ2496_01_standard.jpg', 'GIÀY ADIZERO BOSTON 10', 100, '200.00'),
(23, 'NU006', 2, '200.00', 'ĐÔI GIÀY CHẠY BỘ ĐÀN HỒI DÀNH CHO CẢ CỰ LY NGẮN VÀ DÀI. Buổi chạy của bạn bắt đầu trước cả giây phút bạn sải bước trên đường. Với niềm tin, mục tiêu hay khao khát đạt được phong độ tốt nhất. Hãy mang đôi giày chạy bộ adidas này và bắt đầu buổi chạy. Các yếu tố hiệu năng cao như lớp đệm đàn hồi và lớp phủ ngoài nâng đỡ mang đến sự thoải mái cho sải bước của bạn. Thân giày thoáng khí, không đường may giúp đôi chân bạn luôn thoáng mát kể cả khi tăng tốc.  Sản phẩm này may bằng vải công nghệ Primegreen, thuộc dòng chất liệu tái chế hiệu năng cao. Thân giày chứa 50% thành phần tái chế. Không sử dụng polyester nguyên sinh.', 'https://assets.adidas.com/images/h_840,f_auto,q_auto:sensitive,fl_lossy,c_fill,g_auto/56193edb21db48459638ad0a01255dc9_9366/Giay_Supernova_Tokyo_trang_FY2862_01_standard.jpg', 'GIÀY SUPERNOVA TOKYO', 100, '250.00'),
(24, 'NU007', 2, '100.00', 'ĐÔI GIÀY CHẠY BỘ ĐA NĂNG, NÂNG ĐỠ HỢP TÁC THIẾT KẾ CÙNG KARLIE KLOSS. Dù tâm trí cố gắng kìm hãm bạn. Nhưng đôi chân sẽ giúp bạn giải phóng. Là thành quả hợp tác với Karlie Kloss, đôi giày chạy bộ adidas này có vẻ ngoài táo bạo cho bạn thêm động lực để mang giày và rời nhà vào buổi sáng. Lớp đệm Boost duy trì động lực ấy, với mỗi cú cất bước và chạm đất hoàn trả năng lượng đầy thoải mái.  Sản phẩm này có sử dụng chất liệu tái chế, là một phần cam kết của chúng tôi hướng tới chấm dứt rác thải nhựa. 20% thân giày làm từ chất liệu có chứa tối thiểu 50% thành phần tái chế.', 'https://assets.adidas.com/images/h_840,f_auto,q_auto:sensitive,fl_lossy,c_fill,g_auto/46c30745379346b2a93facc50115d367_9366/Giay_X9000_Karlie_Kloss_Hong_S24028_01_standard.jpg', 'GIÀY X9000 KARLIE KLOSS', 100, '200.00'),
(25, 'NU008', 2, '250.00', 'ĐÔI GIÀY ADIDAS SUPERSTAR THANH THOÁT ĐÓN CHÀO NHỮNG NGÀY HÈ NÓNG NỰC. Đánh thức sự ngọt ngào trong bạn. Tức khắc trở thành một biểu tượng khi ra mắt trên sân bóng rổ vào thập niên 70, dòng giày adidas Superstar nổi tiếng thế giới nay có phiên bản cập nhật thật hấp dẫn. Đôi giày này có 3 Sọc cải biên giúp tăng cường lưu thông khí để đôi chân bạn luôn thoải mái trong tiết trời nóng bức. Hình cây kem ngon mát thêu nổi bật ở gót giày gợi nhớ đến những ngày hè rực nắng và những món ngon giải nhiệt.  Sản phẩm này có sử dụng chất liệu tái chế, là một phần cam kết của chúng tôi hướng tới chấm dứt rác thải nhựa. 20% thân giày làm từ chất liệu có chứa tối thiểu 50% thành phần tái chế.', 'https://assets.adidas.com/images/h_840,f_auto,q_auto:sensitive,fl_lossy,c_fill,g_auto/98acdaf1c0e7471d8579ad0800d504ff_9366/Giay_Superstar_trang_H03895_01_standard.jpg', 'GIÀY SUPERSTAR', 100, '300.00'),
(26, 'NU010', 2, '250.00', 'ĐÔI GIÀY TRAINER VỪA CASUAL VỪA THỂ THAO CHO NHỊP SỐNG NĂNG ĐỘNG. Khi mang đôi giày adidas này, bạn sẵn sàng chạy một vòng công viên rồi sau đó hẹn hò cafe cùng hội bạn. Thân giày bằng vải lưới thoáng khí mang đến cảm giác thoải mái suốt ngày dài. Đế ngoài bằng cao su bền bỉ là bệ đỡ vững chắc cho lịch trình bận rộn của bạn.', 'https://assets.adidas.com/images/h_840,f_auto,q_auto:sensitive,fl_lossy,c_fill,g_auto/478c36ebd3a04f08b58fad1f017a7907_9366/Giay_Run_Falcon_2.0_Mau_tim_H04518_01_standard.jpg', 'GIÀY RUN FALCON 2.0', 100, '300.00'),
(27, 'NU011', 2, '300.00', 'ĐÔI GIÀY CHẠY BỘ THOẢI MÁI CHO SẢI BƯỚC NHẸ NHÀNG. Quyết tâm tăng tốc sau mỗi buổi chạy. Thân giày bằng vải lưới và gót giày bằng cao su neoprene mềm mại đem đến cho đôi giày chạy bộ này vẻ ngoài đậm chất kỹ thuật và độ ôm thoải mái, nâng đỡ. Khi bạn đã sẵn sàng bứt tốc, đế giữa Lightmotion siêu nhẹ cũng sẵn sàng đáp ứng.', 'https://assets.adidas.com/images/h_840,f_auto,q_auto:sensitive,fl_lossy,c_fill,g_auto/eb0fe98677554b47a297ad20000d5664_9366/Giay_Duramo_SL_trang_H04631_01_standard.jpg', 'GIÀY DURAMO SL', 100, '350.00'),
(28, 'GIÀY SUPERSTAR HER STUDIO LONDON', 2, '300.00', 'HER STUDIO LONDON BIẾN HÓA ĐÔI GIÀY TRAINER ADIDAS SUPERSTAR HUYỀN THOẠI. Có lẽ thật khó để một biểu tượng luôn giữ được ấn tượng và cảm giác mới mẻ sau 50 năm. Nhưng câu chuyện về đôi giày adidas Superstar lại luôn đề cao thiết kế phá cách, và HER Studio London đã mang đến phong cách đặc trưng của họ để viết nên một vài chương mới. Các chi tiết kẻ ca rô nổi bật trên 3 Sọc và gót giày, bởi HER Studio London luôn gắn liền với gu họa tiết đặc trưng của họ.  Sản phẩm này có sử dụng chất liệu tái chế, là một phần quyết tâm của chúng tôi hướng tới chấm dứt rác thải nhựa. 20% thân giày làm từ chất liệu có chứa tối thiểu 50% thành phần tái chế.', 'https://assets.adidas.com/images/h_840,f_auto,q_auto:sensitive,fl_lossy,c_fill,g_auto/aa546fbbba9c46d1b983acb800d52b70_9366/Giay_Superstar_HER_Studio_London_trang_H04077_01_standard.jpg', 'NU012', 100, '400.00');

-- --------------------------------------------------------

--
-- Table structure for table `product_lines`
--

CREATE TABLE `product_lines` (
  `id` bigint(20) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `product_line` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_lines`
--

INSERT INTO `product_lines` (`id`, `description`, `product_line`) VALUES
(1, 'Giay danh cho nam gioi', 'GIAY NAM'),
(2, 'Giay danh cho nu gioi', 'GIAY NU');

-- --------------------------------------------------------

--
-- Table structure for table `t_permission`
--

CREATE TABLE `t_permission` (
  `id` bigint(20) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT 0,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `permission_key` varchar(255) DEFAULT NULL,
  `permission_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_permission`
--

INSERT INTO `t_permission` (`id`, `created_at`, `created_by`, `deleted`, `updated_at`, `updated_by`, `permission_key`, `permission_name`) VALUES
(1, NULL, NULL, 0, NULL, NULL, 'CREATE', 'tạo user'),
(2, NULL, NULL, 0, NULL, NULL, 'READ', 'xem user'),
(3, NULL, NULL, 0, NULL, NULL, 'UPDATE', 'sửa user'),
(4, NULL, NULL, 0, NULL, NULL, 'DELETE', 'xóa user');

-- --------------------------------------------------------

--
-- Table structure for table `t_role`
--

CREATE TABLE `t_role` (
  `id` bigint(20) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT 0,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `role_key` varchar(255) DEFAULT NULL,
  `role_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_role`
--

INSERT INTO `t_role` (`id`, `created_at`, `created_by`, `deleted`, `updated_at`, `updated_by`, `role_key`, `role_name`) VALUES
(1, NULL, NULL, 0, NULL, NULL, 'ADMIN', 'Supper User'),
(2, NULL, NULL, 0, NULL, NULL, 'CUSTOMER', 'Khách'),
(3, NULL, NULL, 0, NULL, NULL, 'MANAGER', 'Quan ly');

-- --------------------------------------------------------

--
-- Table structure for table `t_role_permission`
--

CREATE TABLE `t_role_permission` (
  `role_id` bigint(20) NOT NULL,
  `permission_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `t_token`
--

CREATE TABLE `t_token` (
  `id` bigint(20) NOT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `token` varchar(1000) DEFAULT NULL,
  `token_exp_date` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_token`
--

INSERT INTO `t_token` (`id`, `created_by`, `created_at`, `updated_by`, `updated_at`, `token`, `token_exp_date`, `deleted`) VALUES
(10, NULL, NULL, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMwODA5NjgsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkaWpOTjRJT2dsR2FBd3hJaENKVE42dVwvdzh3VDN5RmhPdW9yOHpUNzh1OThxVDI0SVpycGZlIiwidXNlcklkIjoyLCJhdXRob3JpdGllcyI6W10sInVzZXJuYW1lIjoiSHV5RFYifX0.j_DJEsz2jNgTHMRNCtKK3Th11W0ke35nBt1Rjc29ZwY', '2021-10-01 16:36:08', 0),
(11, 'HuyDV', NULL, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMwODEyMDcsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkaWpOTjRJT2dsR2FBd3hJaENKVE42dVwvdzh3VDN5RmhPdW9yOHpUNzh1OThxVDI0SVpycGZlIiwidXNlcklkIjoyLCJhdXRob3JpdGllcyI6W10sInVzZXJuYW1lIjoiSHV5RFYifX0.hwrL-o3H1vqglkXVEfok4dSV5tafGwzq9QF1vdnKYSQ', '2021-10-01 16:40:07', 0),
(12, 'HuyDV', NULL, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMwODEzNjYsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkaWpOTjRJT2dsR2FBd3hJaENKVE42dVwvdzh3VDN5RmhPdW9yOHpUNzh1OThxVDI0SVpycGZlIiwidXNlcklkIjoyLCJhdXRob3JpdGllcyI6W10sInVzZXJuYW1lIjoiSHV5RFYifX0.95I07P69oWEcSABsQGKLkgow-T2QToyYa_cpz_cP5ps', '2021-10-01 16:42:46', 0),
(13, 'HuyDV', NULL, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMwODE1NTMsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkaWpOTjRJT2dsR2FBd3hJaENKVE42dVwvdzh3VDN5RmhPdW9yOHpUNzh1OThxVDI0SVpycGZlIiwidXNlcklkIjoyLCJhdXRob3JpdGllcyI6W10sInVzZXJuYW1lIjoiSHV5RFYifX0.rmsuRevImbb-iMlmy7Eo160QLQB410fbWnFT8EtJGDI', '2021-10-01 16:45:53', 0),
(14, 'HuyDV', NULL, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMwODE1ODUsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkaWpOTjRJT2dsR2FBd3hJaENKVE42dVwvdzh3VDN5RmhPdW9yOHpUNzh1OThxVDI0SVpycGZlIiwidXNlcklkIjoyLCJhdXRob3JpdGllcyI6W10sInVzZXJuYW1lIjoiSHV5RFYifX0.IY2IOAZO6-dVkvCu33FfbWrWhS5o8HKRaMk_ZAloB3U', '2021-10-01 16:46:25', 0),
(15, 'HuyDV', NULL, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMwODE2MjksInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkaWpOTjRJT2dsR2FBd3hJaENKVE42dVwvdzh3VDN5RmhPdW9yOHpUNzh1OThxVDI0SVpycGZlIiwidXNlcklkIjoyLCJhdXRob3JpdGllcyI6W10sInVzZXJuYW1lIjoiSHV5RFYifX0._Ex43ASf-4LTAa5rPS-Kq2m1vkf1OGMjVC2-4v0qkLs', '2021-10-01 16:47:09', 0),
(16, 'HuyDV', NULL, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMwODE2MzUsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkaWpOTjRJT2dsR2FBd3hJaENKVE42dVwvdzh3VDN5RmhPdW9yOHpUNzh1OThxVDI0SVpycGZlIiwidXNlcklkIjoyLCJhdXRob3JpdGllcyI6W10sInVzZXJuYW1lIjoiSHV5RFYifX0.GQow2XOd7vNWbI9hwsCg8efiqQgl6C_sHW-YuU-mUQA', '2021-10-01 16:47:15', 0),
(17, 'HuyDV', NULL, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMwODE2NjIsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkaWpOTjRJT2dsR2FBd3hJaENKVE42dVwvdzh3VDN5RmhPdW9yOHpUNzh1OThxVDI0SVpycGZlIiwidXNlcklkIjoyLCJhdXRob3JpdGllcyI6W10sInVzZXJuYW1lIjoiSHV5RFYifX0.CX9pmGdf8qfXzm5ap2uZBar7G4fxbzHydFBufncfLos', '2021-10-01 16:47:42', 0),
(18, 'HuyDV', NULL, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMwODE2OTAsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkaWpOTjRJT2dsR2FBd3hJaENKVE42dVwvdzh3VDN5RmhPdW9yOHpUNzh1OThxVDI0SVpycGZlIiwidXNlcklkIjoyLCJhdXRob3JpdGllcyI6W10sInVzZXJuYW1lIjoiSHV5RFYifX0.zgkA4HTLLiGVFbQ_tOPPHc_z_w-Y2jy2KSStDRXExKc', '2021-10-01 16:48:10', 0),
(19, 'HuyDV', NULL, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMxMDU4NjgsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkaWpOTjRJT2dsR2FBd3hJaENKVE42dVwvdzh3VDN5RmhPdW9yOHpUNzh1OThxVDI0SVpycGZlIiwidXNlcklkIjoyLCJhdXRob3JpdGllcyI6W10sInVzZXJuYW1lIjoiSHV5RFYifX0.NYjNpn8GnCOSJlMFHmQJYIsQyT4_78mlc_i6zJ0_J4U', '2021-10-01 23:31:08', 0),
(20, 'NhuMay', NULL, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMxMDgwMDcsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkcjk4Rzloa2M5NUFaOFNkZ3VxRTVTT2JIb1BJWGJCa0VsZWJubXdwMThyNWR5MVdGWVZ5NmkiLCJ1c2VySWQiOjQsImF1dGhvcml0aWVzIjpbXSwidXNlcm5hbWUiOiJOaHVNYXkifX0.U5tR203nbcgD8pHhWdPWej1gYh9Saaz5F6sUc8tQhKw', '2021-10-02 00:06:47', 0),
(21, 'NhuMay', NULL, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMyODQ1NzIsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkcjk4Rzloa2M5NUFaOFNkZ3VxRTVTT2JIb1BJWGJCa0VsZWJubXdwMThyNWR5MVdGWVZ5NmkiLCJ1c2VySWQiOjQsImF1dGhvcml0aWVzIjpbXSwidXNlcm5hbWUiOiJOaHVNYXkifX0.CzPGMduQnLvqVg24JLbbxWaDJ30iYWoBl6OATlwEuhU', '2021-10-04 01:09:32', 0),
(22, 'HuyDV', NULL, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMyODkyNzEsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkaWpOTjRJT2dsR2FBd3hJaENKVE42dVwvdzh3VDN5RmhPdW9yOHpUNzh1OThxVDI0SVpycGZlIiwidXNlcklkIjoyLCJhdXRob3JpdGllcyI6W10sInVzZXJuYW1lIjoiSHV5RFYifX0.OY_HPAxl7oY9wN6YzuVZ1kB_5smjcrbYLA8rw3bITbU', '2021-10-04 02:27:51', 0),
(23, 'HuyDV', NULL, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMyODk3MzEsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkaWpOTjRJT2dsR2FBd3hJaENKVE42dVwvdzh3VDN5RmhPdW9yOHpUNzh1OThxVDI0SVpycGZlIiwidXNlcklkIjoyLCJhdXRob3JpdGllcyI6W10sInVzZXJuYW1lIjoiSHV5RFYifX0.UHy8C4JmrB6oWDFOuT7G_gKEbWQUuFdENij8gES86a8', '2021-10-04 02:35:31', 0),
(24, 'BaQuat', NULL, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzM0MjU3OTUsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkWWM3cnA2TFFnZlRZXC91ZE1Ka0VWQmVGZ0hwQUNUcDljNXNNUUM0cWg1eHNxcW10UFpCdm42IiwidXNlcklkIjo2LCJhdXRob3JpdGllcyI6W10sInVzZXJuYW1lIjoiQmFRdWF0In19.FGWhNsBRXmBDOst8cEahm-FiRI-hR36cb05n7SW3ivQ', '2021-10-05 16:23:15', 0),
(25, 'HuyDV', NULL, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzM0Mjc5NDMsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkaWpOTjRJT2dsR2FBd3hJaENKVE42dVwvdzh3VDN5RmhPdW9yOHpUNzh1OThxVDI0SVpycGZlIiwidXNlcklkIjoyLCJhdXRob3JpdGllcyI6W10sInVzZXJuYW1lIjoiSHV5RFYifX0.PFsJMASM0uI6Vm0n6gGOYk7yKiO75jwWNqI_Xo5jF5Y', '2021-10-05 16:59:03', 0),
(26, 'NhuMay', NULL, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzM0MjkyMjEsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkcjk4Rzloa2M5NUFaOFNkZ3VxRTVTT2JIb1BJWGJCa0VsZWJubXdwMThyNWR5MVdGWVZ5NmkiLCJ1c2VySWQiOjQsImF1dGhvcml0aWVzIjpbXSwidXNlcm5hbWUiOiJOaHVNYXkifX0._dkZorhEqd8JDT5MkGVJhIpbCS5II8Z43JZtADmU5IM', '2021-10-05 17:20:21', 0),
(27, 'HuyDV', NULL, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzM0NTkwMjEsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkaWpOTjRJT2dsR2FBd3hJaENKVE42dVwvdzh3VDN5RmhPdW9yOHpUNzh1OThxVDI0SVpycGZlIiwidXNlcklkIjoyLCJhdXRob3JpdGllcyI6W10sInVzZXJuYW1lIjoiSHV5RFYifX0.LboAj5sUZxQMB_fKZ69d04cKfBSzMqAXLnSl5e2zQmI', '2021-10-06 01:37:01', 0),
(28, 'HuyDV', NULL, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzM1MTQwMjQsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkaWpOTjRJT2dsR2FBd3hJaENKVE42dVwvdzh3VDN5RmhPdW9yOHpUNzh1OThxVDI0SVpycGZlIiwidXNlcklkIjoyLCJhdXRob3JpdGllcyI6W10sInVzZXJuYW1lIjoiSHV5RFYifX0.b583F85bHYjg2fArZ3mfyL_b_36SUaZw9sZ2lRVL43Y', '2021-10-06 16:53:44', 0),
(29, 'NhuMay', NULL, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzM1MTQzNjIsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkcjk4Rzloa2M5NUFaOFNkZ3VxRTVTT2JIb1BJWGJCa0VsZWJubXdwMThyNWR5MVdGWVZ5NmkiLCJ1c2VySWQiOjQsImF1dGhvcml0aWVzIjpbXSwidXNlcm5hbWUiOiJOaHVNYXkifX0.RBsZZqprCkxLV-89GqDkfrfnYpD7XHgyYtwp2tJv23g', '2021-10-06 16:59:22', 0),
(30, 'NhuMay', NULL, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzM1MTQ2NjYsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkcjk4Rzloa2M5NUFaOFNkZ3VxRTVTT2JIb1BJWGJCa0VsZWJubXdwMThyNWR5MVdGWVZ5NmkiLCJ1c2VySWQiOjQsImF1dGhvcml0aWVzIjpbXSwidXNlcm5hbWUiOiJOaHVNYXkifX0.PkDC-AOmHjLDUE1otJ9GV74EdTJomfOaP3xPLHKwZZ8', '2021-10-06 17:04:26', 0),
(31, 'NhuMay', NULL, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzM1MTQ5MzcsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkcjk4Rzloa2M5NUFaOFNkZ3VxRTVTT2JIb1BJWGJCa0VsZWJubXdwMThyNWR5MVdGWVZ5NmkiLCJ1c2VySWQiOjQsImF1dGhvcml0aWVzIjpbXSwidXNlcm5hbWUiOiJOaHVNYXkifX0.xmYRXoG-QTrN2VrqfkgETUb0YaqxgNyiGhpIq2hXguc', '2021-10-06 17:08:57', 0),
(32, 'NhuMay', NULL, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzM1MTUwNDAsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkcjk4Rzloa2M5NUFaOFNkZ3VxRTVTT2JIb1BJWGJCa0VsZWJubXdwMThyNWR5MVdGWVZ5NmkiLCJ1c2VySWQiOjQsImF1dGhvcml0aWVzIjpbXSwidXNlcm5hbWUiOiJOaHVNYXkifX0.-FbJZS16hswAF0cAqb6_ntl9ng5qODgwhgw3aHbAHlM', '2021-10-06 17:10:40', 0),
(33, 'NhuMay', NULL, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzM1MTUzMTUsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkcjk4Rzloa2M5NUFaOFNkZ3VxRTVTT2JIb1BJWGJCa0VsZWJubXdwMThyNWR5MVdGWVZ5NmkiLCJ1c2VySWQiOjQsImF1dGhvcml0aWVzIjpbXSwidXNlcm5hbWUiOiJOaHVNYXkifX0.wcTU-hy3JwJh2jIMwdMJTdCMBciEC98TSAOIK_tinpc', '2021-10-06 17:15:15', 0),
(34, 'BaQuat', NULL, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzM1MTUzODAsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkWWM3cnA2TFFnZlRZXC91ZE1Ka0VWQmVGZ0hwQUNUcDljNXNNUUM0cWg1eHNxcW10UFpCdm42IiwidXNlcklkIjo2LCJhdXRob3JpdGllcyI6W10sInVzZXJuYW1lIjoiQmFRdWF0In19.Wbca-1ni62ZymhQyKSr-B9BkYuMuoeVbny9ZXuebRn0', '2021-10-06 17:16:20', 0),
(35, 'BaQuat', NULL, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzM1MTU0MjcsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkWWM3cnA2TFFnZlRZXC91ZE1Ka0VWQmVGZ0hwQUNUcDljNXNNUUM0cWg1eHNxcW10UFpCdm42IiwidXNlcklkIjo2LCJhdXRob3JpdGllcyI6W10sInVzZXJuYW1lIjoiQmFRdWF0In19.VJ5BOcJc2QwYXYsuAWWbY85J8ngCu5-_DMKMaG0vaz4', '2021-10-06 17:17:07', 0),
(36, 'BaQuat', NULL, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzM1MTU0MzgsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkWWM3cnA2TFFnZlRZXC91ZE1Ka0VWQmVGZ0hwQUNUcDljNXNNUUM0cWg1eHNxcW10UFpCdm42IiwidXNlcklkIjo2LCJhdXRob3JpdGllcyI6W10sInVzZXJuYW1lIjoiQmFRdWF0In19.HZzBUAetlbNoGl6dJnxup8NCeMxaOHc9fl5UKg7JLV8', '2021-10-06 17:17:18', 0),
(37, 'BaQuat', NULL, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzM1MTU0NjIsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkWWM3cnA2TFFnZlRZXC91ZE1Ka0VWQmVGZ0hwQUNUcDljNXNNUUM0cWg1eHNxcW10UFpCdm42IiwidXNlcklkIjo2LCJhdXRob3JpdGllcyI6W10sInVzZXJuYW1lIjoiQmFRdWF0In19.1laY54hj8vN7Qczh0a-8hopbfL7zRWkWBqyc1xHRn5Y', '2021-10-06 17:17:42', 0),
(38, 'BaQuat', NULL, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzM1MTU5ODksInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkWWM3cnA2TFFnZlRZXC91ZE1Ka0VWQmVGZ0hwQUNUcDljNXNNUUM0cWg1eHNxcW10UFpCdm42IiwidXNlcklkIjo2LCJhdXRob3JpdGllcyI6W10sInVzZXJuYW1lIjoiQmFRdWF0In19.YrIn6w7djGex2oQsCdRyjeSDo4G_EQ-EMEozxi8dTWg', '2021-10-06 17:26:29', 0),
(39, 'BaQuat', NULL, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzM1MTYwMDIsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkWWM3cnA2TFFnZlRZXC91ZE1Ka0VWQmVGZ0hwQUNUcDljNXNNUUM0cWg1eHNxcW10UFpCdm42IiwidXNlcklkIjo2LCJhdXRob3JpdGllcyI6W10sInVzZXJuYW1lIjoiQmFRdWF0In19.AkR3FXgasNEz4tl7k9PCvFfa3nmzPLHyRRWcw6xQ6S4', '2021-10-06 17:26:42', 0),
(40, 'BaQuat', NULL, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzM1MTYwMjgsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkWWM3cnA2TFFnZlRZXC91ZE1Ka0VWQmVGZ0hwQUNUcDljNXNNUUM0cWg1eHNxcW10UFpCdm42IiwidXNlcklkIjo2LCJhdXRob3JpdGllcyI6W10sInVzZXJuYW1lIjoiQmFRdWF0In19.fIJdJ7HQcJM5SxS_KEEHKrmC89c-99hJOTJ9fpEEeTM', '2021-10-06 17:27:08', 0),
(41, 'BaQuat', NULL, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzM1MTgzODgsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkWWM3cnA2TFFnZlRZXC91ZE1Ka0VWQmVGZ0hwQUNUcDljNXNNUUM0cWg1eHNxcW10UFpCdm42IiwidXNlcklkIjo2LCJhdXRob3JpdGllcyI6W10sInVzZXJuYW1lIjoiQmFRdWF0In19.tPykyWXWXXq7_OY9vBOtAGc_bqpcCBsRv79I8AzUfFM', '2021-10-06 18:06:28', 0),
(42, 'BaQuat', NULL, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzM1MzUzMDksInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkWWM3cnA2TFFnZlRZXC91ZE1Ka0VWQmVGZ0hwQUNUcDljNXNNUUM0cWg1eHNxcW10UFpCdm42IiwidXNlcklkIjo2LCJhdXRob3JpdGllcyI6W10sInVzZXJuYW1lIjoiQmFRdWF0In19._o0acH4fw-ERk7ZzlWzhp2K62TxD7ws8MZ2d7CGY3JI', '2021-10-06 22:48:29', 0),
(43, 'BaQuat', NULL, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzM1MzU1NjAsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkWWM3cnA2TFFnZlRZXC91ZE1Ka0VWQmVGZ0hwQUNUcDljNXNNUUM0cWg1eHNxcW10UFpCdm42IiwidXNlcklkIjo2LCJhdXRob3JpdGllcyI6W10sInVzZXJuYW1lIjoiQmFRdWF0In19.JAcGdlxgwZAqc4I4lxHOQhPutd4VQWhH5ZTtgLwUVBw', '2021-10-06 22:52:40', 0),
(44, 'BaQuat', NULL, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzM1Mzg0MzksInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkWWM3cnA2TFFnZlRZXC91ZE1Ka0VWQmVGZ0hwQUNUcDljNXNNUUM0cWg1eHNxcW10UFpCdm42IiwidXNlcklkIjo2LCJhdXRob3JpdGllcyI6W10sInVzZXJuYW1lIjoiQmFRdWF0In19.1jDMpQxRVcZUf5xL2F89qqBFy3DnrOW1v4eNa7mXULI', '2021-10-06 23:40:39', 0),
(45, 'BaQuat', NULL, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzM1Mzg3MTQsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkWWM3cnA2TFFnZlRZXC91ZE1Ka0VWQmVGZ0hwQUNUcDljNXNNUUM0cWg1eHNxcW10UFpCdm42IiwidXNlcklkIjo2LCJhdXRob3JpdGllcyI6W10sInVzZXJuYW1lIjoiQmFRdWF0In19.oH0sboGv-Ys7mys2zDvTFeEugAJQwGOYHOkjJ04WWtE', '2021-10-06 23:45:14', 0),
(46, 'BaQuat', NULL, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzM1Mzg3MjYsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkWWM3cnA2TFFnZlRZXC91ZE1Ka0VWQmVGZ0hwQUNUcDljNXNNUUM0cWg1eHNxcW10UFpCdm42IiwidXNlcklkIjo2LCJhdXRob3JpdGllcyI6W10sInVzZXJuYW1lIjoiQmFRdWF0In19.vb63ZmFLnBTv67XjeouKRhAQTNmYldTRnoLDat8bbss', '2021-10-06 23:45:26', 0),
(47, 'BaQuat', NULL, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzM1Mzg3MzAsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkWWM3cnA2TFFnZlRZXC91ZE1Ka0VWQmVGZ0hwQUNUcDljNXNNUUM0cWg1eHNxcW10UFpCdm42IiwidXNlcklkIjo2LCJhdXRob3JpdGllcyI6W10sInVzZXJuYW1lIjoiQmFRdWF0In19.3m3xrm4tZbCjhM6NyuRMxlLk9sjMTlyKlfLQBkJg3yU', '2021-10-06 23:45:30', 0),
(48, 'BaQuat', NULL, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzM1Mzg3MzUsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkWWM3cnA2TFFnZlRZXC91ZE1Ka0VWQmVGZ0hwQUNUcDljNXNNUUM0cWg1eHNxcW10UFpCdm42IiwidXNlcklkIjo2LCJhdXRob3JpdGllcyI6W10sInVzZXJuYW1lIjoiQmFRdWF0In19.pMqfv7x0C-3shgl7Hkoy98elQ6NWhtTl0Cd3Qsnltek', '2021-10-06 23:45:35', 0),
(49, 'BaQuat', NULL, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzM1Mzg4NzksInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkWWM3cnA2TFFnZlRZXC91ZE1Ka0VWQmVGZ0hwQUNUcDljNXNNUUM0cWg1eHNxcW10UFpCdm42IiwidXNlcklkIjo2LCJhdXRob3JpdGllcyI6W10sInVzZXJuYW1lIjoiQmFRdWF0In19.OibEWIvraYbeOgMKiIW9_kMWSi-_QZGsa9i4gLHoPI8', '2021-10-06 23:47:59', 0),
(50, 'BaQuat', NULL, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzM1MzkzMzUsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkWWM3cnA2TFFnZlRZXC91ZE1Ka0VWQmVGZ0hwQUNUcDljNXNNUUM0cWg1eHNxcW10UFpCdm42IiwidXNlcklkIjo2LCJhdXRob3JpdGllcyI6W10sInVzZXJuYW1lIjoiQmFRdWF0In19.n9PaS0vAoBdTGJU3aW5U1hHghxoX19iS7ikozpe-s8s', '2021-10-06 23:55:35', 0),
(51, 'BaQuat', NULL, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzM1Mzk0MjAsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkWWM3cnA2TFFnZlRZXC91ZE1Ka0VWQmVGZ0hwQUNUcDljNXNNUUM0cWg1eHNxcW10UFpCdm42IiwidXNlcklkIjo2LCJhdXRob3JpdGllcyI6W10sInVzZXJuYW1lIjoiQmFRdWF0In19.d14MAvAOHYOi-HU9aJYxefb5DxXIc8OvIpgYu_N2Cfo', '2021-10-06 23:57:00', 0),
(52, 'BaQuat', NULL, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzM1Mzk1ODIsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkWWM3cnA2TFFnZlRZXC91ZE1Ka0VWQmVGZ0hwQUNUcDljNXNNUUM0cWg1eHNxcW10UFpCdm42IiwidXNlcklkIjo2LCJhdXRob3JpdGllcyI6W10sInVzZXJuYW1lIjoiQmFRdWF0In19.PmVRMNkEmmR_wW2i7RcPZ8QEnH2I9n89_aIH-ZdNORA', '2021-10-06 23:59:42', 0);

-- --------------------------------------------------------

--
-- Table structure for table `t_user`
--

CREATE TABLE `t_user` (
  `id` bigint(20) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT 0,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_user`
--

INSERT INTO `t_user` (`id`, `user_name`, `first_name`, `last_name`, `email`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted`, `password`) VALUES
(2, 'HuyDV', 'Dang', 'Van Huy', 'test@gmail.com', NULL, NULL, NULL, NULL, 0, '$2a$10$ijNN4IOglGaAwxIhCJTN6u/w8wT3yFhOuor8zT78u98qT24IZrpfe'),
(4, 'NhuMay', 'Nhu', 'May', 'nhumay@test.com', NULL, NULL, NULL, NULL, 0, '$2a$10$r98G9hkc95AZ8SdguqE5SObHoPIXbBkElebnmwp18r5dy1WFYVy6i'),
(6, 'BaQuat', 'Cao Ba', 'Quat', 'caobaquat@gmail.com', NULL, NULL, NULL, NULL, 0, '$2a$10$Yc7rp6LQgfTY/udMJkEVBeFgHpACTp9c5sMQC4qh5xsqqmtPZBvn6');

-- --------------------------------------------------------

--
-- Table structure for table `t_user_role`
--

CREATE TABLE `t_user_role` (
  `user_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK6uv0qku8gsu6x1r2jkrtqwjtn` (`product_id`),
  ADD KEY `FKtrxcovm3u7daqh6vpl1rlncor` (`user_id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `phone_number` (`phone_number`),
  ADD KEY `FKfarlvhlnr0c5lqy5jr1fa253r` (`user_id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offices`
--
ALTER TABLE `offices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKpxtb8awmi0dk6smoh2vp1litg` (`customer_id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKjyu2qbqt8gnvno9oe9j2s2ldk` (`order_id`),
  ADD KEY `FK4q98utpd73imf4yhttm3w0eax` (`product_id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK45dp0030s8e3myd8n6ky4e79g` (`customer_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_922x4t23nx64422orei4meb2y` (`product_code`),
  ADD KEY `FK1eicg1yvaxh1gqdp2lsda7vlv` (`product_line_id`);

--
-- Indexes for table `product_lines`
--
ALTER TABLE `product_lines`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_permission`
--
ALTER TABLE `t_permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_role`
--
ALTER TABLE `t_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_role_permission`
--
ALTER TABLE `t_role_permission`
  ADD PRIMARY KEY (`role_id`,`permission_id`),
  ADD KEY `FKjobmrl6dorhlfite4u34hciik` (`permission_id`);

--
-- Indexes for table `t_token`
--
ALTER TABLE `t_token`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_user`
--
ALTER TABLE `t_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_name` (`user_name`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `t_user_role`
--
ALTER TABLE `t_user_role`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `FKa9c8iiy6ut0gnx491fqx4pxam` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `offices`
--
ALTER TABLE `offices`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_permission`
--
ALTER TABLE `t_permission`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `t_role`
--
ALTER TABLE `t_role`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `t_token`
--
ALTER TABLE `t_token`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `t_user`
--
ALTER TABLE `t_user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `FK6uv0qku8gsu6x1r2jkrtqwjtn` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `FKtrxcovm3u7daqh6vpl1rlncor` FOREIGN KEY (`user_id`) REFERENCES `t_user` (`id`);

--
-- Constraints for table `customers`
--
ALTER TABLE `customers`
  ADD CONSTRAINT `FKfarlvhlnr0c5lqy5jr1fa253r` FOREIGN KEY (`user_id`) REFERENCES `t_user` (`id`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `FKpxtb8awmi0dk6smoh2vp1litg` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`);

--
-- Constraints for table `order_details`
--
ALTER TABLE `order_details`
  ADD CONSTRAINT `FK4q98utpd73imf4yhttm3w0eax` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `FKjyu2qbqt8gnvno9oe9j2s2ldk` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`);

--
-- Constraints for table `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `FK45dp0030s8e3myd8n6ky4e79g` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`);

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `FK1eicg1yvaxh1gqdp2lsda7vlv` FOREIGN KEY (`product_line_id`) REFERENCES `product_lines` (`id`);

--
-- Constraints for table `t_role_permission`
--
ALTER TABLE `t_role_permission`
  ADD CONSTRAINT `FK90j038mnbnthgkc17mqnoilu9` FOREIGN KEY (`role_id`) REFERENCES `t_role` (`id`),
  ADD CONSTRAINT `FKjobmrl6dorhlfite4u34hciik` FOREIGN KEY (`permission_id`) REFERENCES `t_permission` (`id`);

--
-- Constraints for table `t_user_role`
--
ALTER TABLE `t_user_role`
  ADD CONSTRAINT `FKa9c8iiy6ut0gnx491fqx4pxam` FOREIGN KEY (`role_id`) REFERENCES `t_role` (`id`),
  ADD CONSTRAINT `FKq5un6x7ecoef5w1n39cop66kl` FOREIGN KEY (`user_id`) REFERENCES `t_user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
