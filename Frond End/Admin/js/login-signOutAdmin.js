
  //Script thuc hien chuc nang xu ly nhiem vu login, logout, hien thi thong tin nguoi dung tren icon login
  //Kiểm tra token nếu có token tức người dùng đã đăng nhập
  const token = getCookie("token");
  if (!token) {
    //bien lay gia tri cua url hien tai
    var gUrlString = window.location.href;
    //gan URl cua trang hien tai vao localStorage
    localStorage.setItem("curentUrl", gUrlString);
    //Goi toi trang dang nhap
    window.location.href = "Admin-SignIn.html";
  }else{
    var vUserName = localStorage.getItem("userName")
    $("#customer_login").html(vUserName);
  }

  //Hàm get Cookie, co chuc nang kiem tra xem trang da ton tai cookie hay chua
  function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }
  
  //click signOut
  $("#goSignOut").on("click", function () {
    "use strict";
    //Xoa du lieu lien quan trong local Storage
    localStorage.removeItem("userName");
    localStorage.removeItem("userId");
    setCookie('token', '', 0);
    location.href = localStorage.getItem("curentUrl");
  })

  //Ham co chuc nang xoa du lieu cookie tren trinh duyet
  function setCookie(cname, cvalue, exdays) {
    "use strict";
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = " expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }