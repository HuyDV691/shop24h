package com.devcamp.shop24h.apiCreateUser;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import com.devcamp.shop24h.model.User;
import com.devcamp.shop24h.repository.UserRepository;
import com.devcamp.shop24h.security.UserPrincipal;
import com.devcamp.shop24h.service.UserService;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;


/**
 * @author hieuha
 *
 */
@RestController
public class WithoutAuthorizeController {
	@Autowired
	private UserRepository userRepository;
	
    /**
     * Test trường hợp khôngcheck quyền Authorize lấy là danh sách user
     * @return
     */
    @GetMapping("/users")
    public ResponseEntity<List<Object>> getUsers() {
        return new ResponseEntity(userRepository.findAll(),HttpStatus.OK);
    }
    
    //Code lay thong tin user theo role
    @GetMapping("/users/roles")
    public ResponseEntity<Object> getUserRoles() {
    	Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    	UserPrincipal userPrincipal = ((UserPrincipal) authentication.getPrincipal());
    	Collection<GrantedAuthority> authorities = userPrincipal.getAuthorities();
    	Set<String> roles = authentication.getAuthorities().stream()
    	.map(r -> r.getAuthority()).collect(Collectors.toSet());
    	boolean hasUserRole = authentication.getAuthorities().stream()
    	.anyMatch(r -> r.getAuthority().equals("ROLE_USER"));
        return new ResponseEntity(authorities,HttpStatus.OK);
    }
    /**
     * Test trường hợp khôngcheck quyền Authorize
     * Tạo mới user
     * @param user
     * @return
     */
    
    @PostMapping("/users/createAdmin")
    @CrossOrigin
    public ResponseEntity<Object> create(@RequestBody User user) {
    	user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
    	return new ResponseEntity(userRepository.save(user),HttpStatus.CREATED);
    }
    
    @Autowired
    private UserService userService;
    /**
     * Test có kiểm tra quyền.
     * @param user
     * @return
     */
    @PostMapping("/users/create")
    @PreAuthorize("hasAnyAuthority('USER_CREATE')")
    @CrossOrigin
    public User register(@RequestBody User user) {
        user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));

        return userService.createUser(user);
    }
}
