package com.devcamp.shop24h.model;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;

@Entity
@Table(name = "customers")
public class CCustomer {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "last_name")
	private String lastName;
	
	@Column(name = "first_name")
	private String firstName;
	
	@Column(name = "phone_number", unique = true)
	private String phoneNumber;
	
	@Column(name = "address")
	private String address;
	
	@Column(name = "city")
	private String city;
	
	@Column(name = "state")
	private String state;
	
	@Column(name = "postal_code")
	private String postalCode;

	@Column(name = "country")
	private String country;

	@Column(name = "sales_rep_employee_number")
	private int salesRepEmployeeNumber;
	
	@Column(name = "credit_limit")
	private int creditLimit;

	@OneToMany(targetEntity = COrder.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "customer_id")
	@JsonIgnore
	private List<COrder> cOrders;

	@OneToMany(targetEntity = CPayment.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "customer_id")
	@JsonIgnore
	private List<CPayment> cPayments;
	
	@OneToOne
	@JsonIgnore
	private User user;
	
	public CCustomer() {
	}

	public CCustomer(Long id, String address, String city, String country, int creditLimit, String firstName,
			String lastName, String phoneNumber, String postalCode, int salesRepEmployeeNumber, String state,
			List<COrder> cOrders, List<CPayment> cPayments) {
		super();
		this.id = id;
		this.address = address;
		this.city = city;
		this.country = country;
		this.creditLimit = creditLimit;
		this.firstName = firstName;
		this.lastName = lastName;
		this.phoneNumber = phoneNumber;
		this.postalCode = postalCode;
		this.salesRepEmployeeNumber = salesRepEmployeeNumber;
		this.state = state;
		this.cOrders = cOrders;
		this.cPayments = cPayments;
	}

	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public int getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(int creditLimit) {
		this.creditLimit = creditLimit;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public int getSalesRepEmployeeNumber() {
		return salesRepEmployeeNumber;
	}

	public void setSalesRepEmployeeNumber(int salesRepEmployeeNumber) {
		this.salesRepEmployeeNumber = salesRepEmployeeNumber;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public List<COrder> getcOrders() {
		return cOrders;
	}

	public void setcOrders(List<COrder> cOrders) {
		this.cOrders = cOrders;
	}

	public List<CPayment> getcPayments() {
		return cPayments;
	}

	public void setcPayments(List<CPayment> cPayments) {
		this.cPayments = cPayments;
	}

}