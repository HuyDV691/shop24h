package com.devcamp.shop24h.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "payments")
public class CPayment {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

//	@Column(name = "customer_id")
//	private int custommerId;

	@Column(name = "ammount")
	private BigDecimal ammount;

	@Column(name = "check_number")
	private String checkNumber;

	@Temporal(TemporalType.DATE)
	@Column(name = "payment_date")
	private Date paymentDate;

	@ManyToOne
	private CCustomer customer;

//	@Transient
//	private String phoneNumber;
//	
	public CPayment() {
	}

	public CPayment(Long id, int custommerId, BigDecimal ammount, String checkNumber, Date paymentDate,
			CCustomer customer) {
		super();
		this.id = id;
		//this.custommerId = custommerId;
		this.ammount = ammount;
		this.checkNumber = checkNumber;
		this.paymentDate = paymentDate;
		this.customer = customer;
	}

	
//	public String getPhoneNumber() {
//		return getCustomer().getPhoneNumber();
//	}
//
//	public void setPhoneNumber(String phoneNumber) {
//		this.phoneNumber = phoneNumber;
//	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

//	public int getCustommerId() {
//		return custommerId;
//	}
//
//	public void setCustommerId(int custommerId) {
//		this.custommerId = custommerId;
//	}

	public CCustomer getCustomer() {
		return customer;
	}

	public void setCustomer(CCustomer customer) {
		this.customer = customer;
	}

	public BigDecimal getAmmount() {
		return ammount;
	}

	public void setAmmount(BigDecimal ammount) {
		this.ammount = ammount;
	}

	public String getCheckNumber() {
		return checkNumber;
	}

	public void setCheckNumber(String checkNumber) {
		this.checkNumber = checkNumber;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}
}