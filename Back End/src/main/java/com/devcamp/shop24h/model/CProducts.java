package com.devcamp.shop24h.model;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "products")
public class CProducts {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "product_name")
	private String productName;

	@Column(name = "product_description")
	private String productDescription;


	@Column(name = "product_image")
	private String productImage;

	@Column(name = "quantity_in_stock")
	private int quantityInStock;

	@Column(name = "buy_price")
	private BigDecimal buyPrice;

	@Column(name = "sell_price")
	private BigDecimal sellPrice;

	@Column(name = "product_code", unique = true)
	private String productCode;

	@ManyToOne
	private CProductLines product_line;

	@OneToMany(targetEntity = COrderDetail.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "product_id")
	@JsonIgnore
	private List<COrderDetail> cOrderDetail;
	
	@OneToMany(targetEntity = CComment.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "product_id")
	@JsonIgnore
	private List<CComment> cComment;

//	@Transient
//	private String productLine;

	public CProducts() {
		super();
		// TODO Auto-generated constructor stub
	}



	public CProducts(Long id, String productName, String productDescription, String productImage, int quantityInStock,
		BigDecimal buyPrice, BigDecimal sellPrice, String productCode, CProductLines product_line,
		List<COrderDetail> cOrderDetail, List<CComment> cComment) {
	super();
	this.id = id;
	this.productName = productName;
	this.productDescription = productDescription;
	this.productImage = productImage;
	this.quantityInStock = quantityInStock;
	this.buyPrice = buyPrice;
	this.sellPrice = sellPrice;
	this.productCode = productCode;
	this.product_line = product_line;
	this.cOrderDetail = cOrderDetail;
	this.cComment = cComment;
}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public String getProductImage() {
		return productImage;
	}

	public void setProductImage(String productImage) {
		this.productImage = productImage;
	}

	public int getQuantityInStock() {
		return quantityInStock;
	}

	public void setQuantityInStock(int quantityInStock) {
		this.quantityInStock = quantityInStock;
	}

	public BigDecimal getBuyPrice() {
		return buyPrice;
	}

	public void setBuyPrice(BigDecimal buyPrice) {
		this.buyPrice = buyPrice;
	}

	public BigDecimal getSellPrice() {
		return sellPrice;
	}

	public void setSellPrice(BigDecimal sellPrice) {
		this.sellPrice = sellPrice;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public CProductLines getProductLine() {
		return product_line;
	}

	public void setProductLine(CProductLines product_line) {
		this.product_line = product_line;
	}

	public List<COrderDetail> getcOrderDetail() {
		return cOrderDetail;
	}

	public void setcOrderDetail(List<COrderDetail> cOrderDetail) {
		this.cOrderDetail = cOrderDetail;
	}

	public List<CComment> getcComment() {
		return cComment;
	}

	public void setcComment(List<CComment> cComment) {
		this.cComment = cComment;
	}

	
}
