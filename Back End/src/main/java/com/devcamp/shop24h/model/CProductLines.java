package com.devcamp.shop24h.model;

import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "product_lines")
public class CProductLines {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "product_line")
	private String productLine;
	
	@Column(name = "description")
	private String description;
	
	@OneToMany(targetEntity = CProducts.class, cascade = CascadeType.ALL)
	@JoinColumn(name="product_line_id")
	@JsonIgnore
	private List<CProducts> cProducs;

	public CProductLines() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CProductLines(Long id, String description, String productLine, List<CProducts> cProducs) {
		super();
		this.id = id;
		this.description = description;
		this.productLine = productLine;
		this.cProducs = cProducs;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
	public String getProductLine() {
		return productLine;
	}

	public void setProductLine(String productLine) {
		this.productLine = productLine;
	}

	//@JsonIgnore
	public List<CProducts> getcProducs() {
		return cProducs;
	}

	public void setcProducs(List<CProducts> cProducs) {
		this.cProducs = cProducs;
	}
	
	
}
