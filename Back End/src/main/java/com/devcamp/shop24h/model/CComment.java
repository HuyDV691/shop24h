package com.devcamp.shop24h.model;

import java.math.BigDecimal;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "comments")
public class CComment {

		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private int id;

		@Column(name = "name")
		private String name;

		@Column(name = "comments")
		private String comments;

		@Column(name = "rate_star")
		private java.math.BigDecimal rateStar;

		@ManyToOne
		@JsonIgnore
		private CProducts product;

		@ManyToOne
		@JsonIgnore
		private User user;

		public CComment(int id, String name, String comments, BigDecimal rateStar, CProducts product, User user) {
			super();
			this.id = id;
			this.name = name;
			this.comments = comments;
			this.rateStar = rateStar;
			this.product = product;
			this.user = user;
		}

		public User getUser() {
			return user;
		}

		public void setUser(User user) {
			this.user = user;
		}

		public CComment() {
			super();
			// TODO Auto-generated constructor stub
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getComments() {
			return comments;
		}

		public void setComments(String comments) {
			this.comments = comments;
		}

		public java.math.BigDecimal getRateStar() {
			return rateStar;
		}

		public void setRateStar(java.math.BigDecimal rateStar) {
			this.rateStar = rateStar;
		}

		public CProducts getProduct() {
			return product;
		}

		public void setProduct(CProducts product) {
			this.product = product;
		}

//		@OneToMany(mappedBy = "commentId", fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE })
//		List<ReplyComment> replies;
		
		
}
