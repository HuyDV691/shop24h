package com.devcamp.shop24h.model;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;
import java.util.List;

@Entity
@Table(name = "orders")
public class COrder {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "comments")
	private String comments;

	@Temporal(TemporalType.DATE)
	@Column(name = "order_date")
	private Date orderDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "required_date")
	private Date requiredDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "shipped_date")
	private Date shippedDate;

	@Column(name = "status")
	private String status;
	
	@OneToMany(targetEntity = COrderDetail.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "order_id")
	@JsonIgnore
	private List<COrderDetail> cOrderDetails;

	@ManyToOne
	private CCustomer customer;

	public COrder() {
	}

	public COrder(Long id, String comments, Date orderDate, Date requiredDate, Date shippedDate, String status,
			int customerId, List<COrderDetail> cOrderDetails, CCustomer cCustomer) {
		super();
		this.id = id;
		this.comments = comments;
		this.orderDate = orderDate;
		this.requiredDate = requiredDate;
		this.shippedDate = shippedDate;
		this.status = status;
		this.cOrderDetails = cOrderDetails;
		this.customer = cCustomer;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public Date getRequiredDate() {
		return requiredDate;
	}

	public void setRequiredDate(Date requiredDate) {
		this.requiredDate = requiredDate;
	}

	public Date getShippedDate() {
		return shippedDate;
	}

	public void setShippedDate(Date shippedDate) {
		this.shippedDate = shippedDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<COrderDetail> getcOrderDetails() {
		return cOrderDetails;
	}

	public void setcOrderDetails(List<COrderDetail> cOrderDetails) {
		this.cOrderDetails = cOrderDetails;
	}

	public CCustomer getCustomer() {
		return customer;
	}

	public void setCustomer(CCustomer customer) {
		this.customer = customer;
	}

}