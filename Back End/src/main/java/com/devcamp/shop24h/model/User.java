package com.devcamp.shop24h.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "t_user")
@Getter
@Setter
public class User extends BaseEntity {
	
	@JoinColumn(name = "firstName")
	private String firstName;
	
	@JoinColumn(name = "lastName")
	private String lastName;
	
	@JoinColumn(name = "email", unique = true)
	private String email;
	
	@JoinColumn(name = "userName", unique = true)
    private String userName;
    
    private String password;
    
    @OneToOne(targetEntity = CCustomer.class ,cascade = CascadeType.ALL)
    @JoinColumn(name = "id")
    private CCustomer customer;
    

    @ManyToMany(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    @JoinTable(name = "t_user_role", joinColumns = {@JoinColumn(name = "user_id")}, inverseJoinColumns = {@JoinColumn(name = "role_id")})
    private Set<Role> roles = new HashSet<>();

    @OneToMany(targetEntity = CComment.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "user_id")
	@JsonIgnore
	private List<CComment> cComment;
    
    

	public List<CComment> getcComment() {
		return cComment;
	}

	public void setcComment(List<CComment> cComment) {
		this.cComment = cComment;
	}

	public CCustomer getCustomer() {
		return customer;
	}

	public void setCustomer(CCustomer customer) {
		this.customer = customer;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the username
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param username the username to set
	 */
	public void setUserName(String username) {
		this.userName = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the roles
	 */
	public Set<Role> getRoles() {
		return roles;
	}

	/**
	 * @param roles the roles to set
	 */
	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

}
