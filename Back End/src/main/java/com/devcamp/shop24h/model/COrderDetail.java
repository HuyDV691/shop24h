package com.devcamp.shop24h.model;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name="order_details")
public class COrderDetail  {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name="price_each")
	private BigDecimal priceEach;

	@Column(name="quantity_order")
	private int quantityOrder;

	@ManyToOne
	private CProducts product;

	@ManyToOne
	private COrder order;
	
	public COrderDetail() {
	}
	
	public COrderDetail(Long id, BigDecimal priceEach, int quantityOrder, CProducts product, COrder order) {
		super();
		this.id = id;
		this.priceEach = priceEach;
		this.quantityOrder = quantityOrder;
		this.product = product;
		this.order = order;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getPriceEach() {
		return priceEach;
	}

	public void setPriceEach(BigDecimal priceEach) {
		this.priceEach = priceEach;
	}

	public int getQuantityOrder() {
		return quantityOrder;
	}

	public void setQuantityOrder(int quantityOrder) {
		this.quantityOrder = quantityOrder;
	}

	public CProducts getProduct() {
		return product;
	}

	public void setProduct(CProducts product) {
		this.product = product;
	}

	public COrder getOrder() {
		return order;
	}

	public void setOrder(COrder order) {
		this.order = order;
	}
}