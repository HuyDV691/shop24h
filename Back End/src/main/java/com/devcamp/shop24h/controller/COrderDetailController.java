package com.devcamp.shop24h.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.shop24h.model.COrder;
import com.devcamp.shop24h.model.COrderDetail;
import com.devcamp.shop24h.model.CProducts;
import com.devcamp.shop24h.repository.IOrderDetailRepository;
import com.devcamp.shop24h.repository.IOrderRepository;
import com.devcamp.shop24h.repository.IProductRepository;

@RestController
public class COrderDetailController {

	@Autowired
	private IOrderDetailRepository iOrderDetailRepository;
	@Autowired
	private IProductRepository iProductRepository;
	@Autowired
	private IOrderRepository iOrderRepository;
	//Toan bo code chua chay
	
	// Get all orderDetails information
	@CrossOrigin
	@GetMapping("/orderDetails")
	public ResponseEntity<List<COrderDetail>> getAllOrderDetails() {
		try {
			List<COrderDetail> listAllOrderDetails = new ArrayList<COrderDetail>();
			iOrderDetailRepository.findAll().forEach(listAllOrderDetails::add);
			return new ResponseEntity<>(listAllOrderDetails, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// Get info orderDetails by Id
	@CrossOrigin
	@GetMapping("/orderDetails/{orderDetailId}")
	public COrderDetail getOrderDetailInfoById(@PathVariable Long orderDetailId) {
		if (iOrderDetailRepository.findById(orderDetailId).isPresent())
			return iOrderDetailRepository.findById(orderDetailId).get();
		else
			return null;
	}

	// Delete orderDetails by Id
	@CrossOrigin
	@DeleteMapping("/orderDetails/{orderDetailId}")
	public ResponseEntity<Object> deleteOrderDetailById(@PathVariable Long orderDetailId) {
		try {
			iOrderDetailRepository.deleteById(orderDetailId);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// Delete All orderDetails
	@CrossOrigin
	@DeleteMapping("/orderDetails")
	public ResponseEntity<COrderDetail> deleteAllorderDetails() {
		try {
			iOrderDetailRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	
	// Create new orderDetails
	@CrossOrigin
	@PostMapping("/products/{productId}/{orderId}/orderDetail")
	public ResponseEntity<Object> createNeworderDetails(@PathVariable("productId") Long productId,
			@PathVariable("orderId") Long orderId,	@RequestBody COrderDetail pOrderDetail) {
		try {
			
			Optional<CProducts> productsData = iProductRepository.findById(productId);
			Optional<COrder> orderData = iOrderRepository.findById(orderId);
			if (productsData.isPresent() & orderData.isPresent()) {
				COrderDetail newOrderDetail = new COrderDetail();
				newOrderDetail.setPriceEach(pOrderDetail.getPriceEach());
				newOrderDetail.setQuantityOrder(pOrderDetail.getQuantityOrder());
				//newOrderDetail.setproducts(pOrderDetail.getproducts());

				CProducts products = productsData.get();
				newOrderDetail.setProduct(products);
				
				COrder order = orderData.get();
				newOrderDetail.setOrder(order);
				
				
				COrderDetail saveNewOrderDetail = iOrderDetailRepository.save(newOrderDetail);
				return new ResponseEntity<>(saveNewOrderDetail, HttpStatus.CREATED);
			}else {
				return new ResponseEntity<>("product hoac order khong ton tai", HttpStatus.NOT_FOUND);
			}
			
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create New orderDetails: " + e.getCause().getCause().getMessage());
		}
	}

	// Update orderDetails Exist
	@CrossOrigin
	@PutMapping("/products/{productId}/{orderId}/orderDetail/{orderDetailId}")
	public ResponseEntity<Object> updateOrderDetail( @PathVariable("productId") Long productId,
			@PathVariable("orderDetailId") Long orderDetailId, @RequestBody COrderDetail pOrderDetail,
			@PathVariable("orderId") Long orderId) {
		
		Optional<CProducts> existProduct = iProductRepository.findById(productId);
		Optional<COrder> existOrder = iOrderRepository.findById(orderId);
		Optional<COrderDetail> existOrderDetail = iOrderDetailRepository.findById(orderDetailId);
		
		if (existProduct.isPresent() & existOrderDetail.isPresent() & existOrder.isPresent()) {
			COrderDetail updateOrderDetail = existOrderDetail.get();
			updateOrderDetail.setPriceEach(pOrderDetail.getPriceEach());
			updateOrderDetail.setQuantityOrder(pOrderDetail.getQuantityOrder());
			
			COrderDetail saveUpdateorderDetails = iOrderDetailRepository.save(updateOrderDetail);
			return new ResponseEntity<>(saveUpdateorderDetails, HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Khong ton tai orderDetails de update", HttpStatus.NOT_FOUND);
		}
	}
	
	

}
