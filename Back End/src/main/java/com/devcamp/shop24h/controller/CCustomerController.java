package com.devcamp.shop24h.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.shop24h.model.CCustomer;
import com.devcamp.shop24h.model.COrder;
import com.devcamp.shop24h.model.CPayment;
import com.devcamp.shop24h.model.User;
import com.devcamp.shop24h.repository.*;
import com.devcamp.shop24h.service.ExcelExporter;

import org.springframework.data.domain.PageRequest;

@RestController
public class CCustomerController {

	@Autowired
	private ICustomerRepository iCustomerRepository;
	@Autowired
	private IOrderRepository iOrderRespository;
	@Autowired
	private IPaymentRepository iPaymentRespository;
	@Autowired
	private UserRepository userRepo;

	// Get all customers information
	@CrossOrigin
	@GetMapping("/customers")
	public ResponseEntity<List<CCustomer>> getAllCustomers() {
		try {
			List<CCustomer> listAllCustomers = new ArrayList<CCustomer>();
			iCustomerRepository.findAll().forEach(listAllCustomers::add);
			return new ResponseEntity<>(listAllCustomers, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// Get info customers by Id
	@CrossOrigin
	@GetMapping("/customers/{customerId}")
	public CCustomer getCustomerInfoById(@PathVariable Long customerId) {
		if (iCustomerRepository.findById(customerId).isPresent())
			return iCustomerRepository.findById(customerId).get();
	
		else
			return null;
	}

	// Get info customers Order by Id
	@CrossOrigin
	@GetMapping("/customerOrder/{customerId}")
	public ResponseEntity<Object> getCustomerOrderInfoById(@PathVariable Long customerId) {

		Optional<CCustomer> existCustomer = iCustomerRepository.findById(customerId);
		if (existCustomer.isPresent()) {

			List<COrder> orderDetail = existCustomer.get().getcOrders();
			List<COrder> saveOrderDetail = iOrderRespository.saveAll(orderDetail);

			return new ResponseEntity<>(saveOrderDetail, HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Khong ton tai customers de update", HttpStatus.NOT_FOUND);
		}
	}

	// Get info customers Order by Id
	@CrossOrigin
	@GetMapping("/customerPayments/{customerId}")
	public ResponseEntity<Object> getCustomerPaymentsInfoById(@PathVariable Long customerId) {

		Optional<CCustomer> existCustomer = iCustomerRepository.findById(customerId);
		if (existCustomer.isPresent()) {

			List<CPayment> paymentsDetail = existCustomer.get().getcPayments();

			List<CPayment> savePaymentDetail = iPaymentRespository.saveAll(paymentsDetail);

			return new ResponseEntity<>(savePaymentDetail, HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Khong ton tai customers de update", HttpStatus.NOT_FOUND);
		}
	}

	// Delete customers by Id
	@CrossOrigin
	@DeleteMapping("/customers/{customerId}")
	public ResponseEntity<Object> deletecustomersById(@PathVariable Long customerId) {
		try {
			iCustomerRepository.deleteById(customerId);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// Delete All customers
	@CrossOrigin
	@DeleteMapping("/customers")
	public ResponseEntity<CCustomer> deleteAllcustomers() {
		try {
			iCustomerRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// Create new customer by userId
	@CrossOrigin
	@PostMapping("/user/{userId}/customers")
	public ResponseEntity<Object> createNewCustomerByUserId(@PathVariable("userId") Long userId,
			@RequestBody CCustomer pCustomer) {
		try {
			Optional<User> userData = userRepo.findById(userId);
			if (userData.isPresent()) {
				CCustomer newCustomer = new CCustomer();
				newCustomer.setAddress(pCustomer.getAddress());
				newCustomer.setCity(pCustomer.getCity());
				newCustomer.setCountry(pCustomer.getCountry());
				newCustomer.setCreditLimit(pCustomer.getCreditLimit());
				newCustomer.setFirstName(pCustomer.getFirstName());
				newCustomer.setLastName(pCustomer.getLastName());
				newCustomer.setPhoneNumber(pCustomer.getPhoneNumber());
				newCustomer.setPostalCode(pCustomer.getPostalCode());
				newCustomer.setSalesRepEmployeeNumber(pCustomer.getSalesRepEmployeeNumber());
				newCustomer.setState(pCustomer.getState());
				newCustomer.setcOrders(pCustomer.getcOrders());
				newCustomer.setcPayments(pCustomer.getcPayments());

				User user = userData.get();
				newCustomer.setUser(user);

				CCustomer saveNewCustomer = iCustomerRepository.save(newCustomer);
				return new ResponseEntity<>(saveNewCustomer, HttpStatus.CREATED);
			} else {
				return new ResponseEntity<>("user khong ton tai", HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create New customers: " + e.getCause().getCause().getMessage());
		}
	}

	// Create new customers
	@CrossOrigin
	@PostMapping("/customers")
	public ResponseEntity<Object> createNewCustomer(@RequestBody CCustomer pCustomer) {
		try {
			CCustomer newCustomer = new CCustomer();
			newCustomer.setAddress(pCustomer.getAddress());
			newCustomer.setCity(pCustomer.getCity());
			newCustomer.setCountry(pCustomer.getCountry());
			newCustomer.setCreditLimit(pCustomer.getCreditLimit());
			newCustomer.setFirstName(pCustomer.getFirstName());
			newCustomer.setLastName(pCustomer.getLastName());
			newCustomer.setPhoneNumber(pCustomer.getPhoneNumber());
			newCustomer.setPostalCode(pCustomer.getPostalCode());
			newCustomer.setSalesRepEmployeeNumber(pCustomer.getSalesRepEmployeeNumber());
			newCustomer.setState(pCustomer.getState());
			newCustomer.setcOrders(pCustomer.getcOrders());
			newCustomer.setcPayments(pCustomer.getcPayments());

			CCustomer saveNewCustomer = iCustomerRepository.save(newCustomer);
			return new ResponseEntity<>(saveNewCustomer, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create New customers: " + e.getCause().getCause().getMessage());
		}
	}

	// Update customers Exist
	@CrossOrigin
	@PutMapping("/customers/{customerId}")
	public ResponseEntity<Object> updatecustomers(@PathVariable("customerId") Long customerId,
			@RequestBody CCustomer pCustomer) {
		Optional<CCustomer> existCustomer = iCustomerRepository.findById(customerId);
		if (existCustomer.isPresent()) {
			CCustomer updateCustomer = existCustomer.get();
			updateCustomer.setAddress(pCustomer.getAddress());
			updateCustomer.setCity(pCustomer.getCity());
			updateCustomer.setCountry(pCustomer.getCountry());
			updateCustomer.setCreditLimit(pCustomer.getCreditLimit());
			updateCustomer.setFirstName(pCustomer.getFirstName());
			updateCustomer.setLastName(pCustomer.getLastName());
			updateCustomer.setPhoneNumber(pCustomer.getPhoneNumber());
			updateCustomer.setPostalCode(pCustomer.getPostalCode());
			updateCustomer.setSalesRepEmployeeNumber(pCustomer.getSalesRepEmployeeNumber());
			updateCustomer.setState(pCustomer.getState());

			CCustomer saveUpdateCustomer = iCustomerRepository.save(updateCustomer);
			return new ResponseEntity<>(saveUpdateCustomer, HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Khong ton tai customers de update", HttpStatus.NOT_FOUND);
		}
	}

	// -------------shop24h------------------
	// Controller tim thong tin phone number cua customer, neu trong database ton
	// tai phone number, tra ve id cua customer do,
	// neu trong database khong ton tai phone number, tra ve ket qua null
	@CrossOrigin
	@GetMapping("/customerByPhone/{inputPhoneNumber}")
	public ResponseEntity<Object> getCustomerbyPhoneNumber(@PathVariable String inputPhoneNumber) {
		try {
			List<CCustomer> ExistCustomers = iCustomerRepository.findCustomerByPhoneNumber(inputPhoneNumber);
			return new ResponseEntity<>(ExistCustomers, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// --------------------------------------

	// Task 79.60: Viết query cho bảng customers cho phép tìm danh sách
	// theo họ hoặc tên với LIKE
	@CrossOrigin
	@GetMapping("/customerByName/{inputName}")
	public ResponseEntity<List<CCustomer>> getAllCustomersByName(@PathVariable String inputName) {
		try {
			List<CCustomer> listAllCustomers = new ArrayList<CCustomer>();
			iCustomerRepository.findCustomerByName(inputName).forEach(listAllCustomers::add);
			return new ResponseEntity<>(listAllCustomers, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// Task 79.60:Viết query cho bảng customers cho phép tìm danh sách
	// theo city, state với LIKE có phân trang.
	@CrossOrigin
	@GetMapping("/customerByCity/{inputData}")
	public ResponseEntity<List<CCustomer>> getAllCustomersByCity(@PathVariable String inputData) {
		try {
			List<CCustomer> listAllCustomers = new ArrayList<CCustomer>();
			iCustomerRepository.findCustomerByCity(inputData, PageRequest.of(0, 3)).forEach(listAllCustomers::add);
			return new ResponseEntity<>(listAllCustomers, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// Task 79.60:Viết query cho bảng customers cho phép tìm danh sách
	// theo country có phân trang và ORDER BY tên.
	@CrossOrigin
	@GetMapping("/customerByCountry/{country}")
	public ResponseEntity<List<CCustomer>> getAllCustomersByCountry(@PathVariable String country) {
		try {
			List<CCustomer> listAllCustomers = new ArrayList<CCustomer>();
			iCustomerRepository.findCustomerByCity(country, PageRequest.of(0, 3)).forEach(listAllCustomers::add);
			return new ResponseEntity<>(listAllCustomers, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// Task 79.60:Viết query cho bảng customers cho phép UPDATE dữ liệu có country =
	// NULL
	// với giá trị truyền vào từ tham số

	@CrossOrigin
	@PutMapping("/customerUpdate/{nameCountry}")
	public ResponseEntity<Object> updatecustomers(@PathVariable("nameCountry") String nameCountry) {
		try {
			iCustomerRepository.updateCustomerCountry(nameCountry);
			return new ResponseEntity<>("Du lieu Update thanh cong", HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// ---------------shop24h------------------
	// ---------------Excel Export-------------
	@GetMapping("/export/customers/excel")
	public void exportToExcel(HttpServletResponse response) throws IOException {
		response.setContentType("application/octet-stream");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());

		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=users_" + currentDateTime + ".xlsx";
		response.setHeader(headerKey, headerValue);

		List<CCustomer> customer = new ArrayList<CCustomer>();

		iCustomerRepository.findAll().forEach(customer::add);

		ExcelExporter excelExporter = new ExcelExporter(customer);

		excelExporter.export(response);
	}

	// Lay ra customer theo userId

	@CrossOrigin
	@GetMapping("/user/{userId}/customer")
	public ResponseEntity<Object> getCustomersByUserId(@PathVariable Long userId) {
		try {
			CCustomer Customers = iCustomerRepository.findCustomerByUserId(userId);
			
			return new ResponseEntity<>(Customers, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
