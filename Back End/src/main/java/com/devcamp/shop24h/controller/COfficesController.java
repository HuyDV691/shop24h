package com.devcamp.shop24h.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.shop24h.model.COffices;
import com.devcamp.shop24h.repository.IOfficesRepository;

@RestController
public class COfficesController {

	@Autowired
	private IOfficesRepository iOfficesRepository;

	// Get all Offices information
	@CrossOrigin
	@GetMapping("/Offices")
	public ResponseEntity<List<COffices>> getAllOffices() {
		try {
			List<COffices> listAllOffices = new ArrayList<COffices>();
			iOfficesRepository.findAll().forEach(listAllOffices::add);
			return new ResponseEntity<>(listAllOffices, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// Get info Offices by Id
	@CrossOrigin
	@GetMapping("/Offices/{OfficesId}")
	public COffices getOfficesInfoById(@PathVariable Long OfficesId) {
		if (iOfficesRepository.findById(OfficesId).isPresent())
			return iOfficesRepository.findById(OfficesId).get();
		else
			return null;
	}

	// Delete Offices by Id
	@CrossOrigin
	@DeleteMapping("/Offices/{OfficesId}")
	public ResponseEntity<Object> deleteOfficesById(@PathVariable Long OfficesId) {
		try {
			iOfficesRepository.deleteById(OfficesId);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// Delete All Offices
	@CrossOrigin
	@DeleteMapping("/Offices")
	public ResponseEntity<COffices> deleteAllOffices() {
		try {
			iOfficesRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// Create new Offices
	@CrossOrigin
	@PostMapping("/Offices")
	public ResponseEntity<Object> createNewOffices( @RequestBody COffices pOffices) {
		try {
			COffices newOffices = new COffices();
			newOffices.setCity(pOffices.getCity());
			newOffices.setPhone(pOffices.getPhone());
			newOffices.setAddressLine(pOffices.getAddressLine());
			newOffices.setState(pOffices.getState());
			newOffices.setCountry(pOffices.getCountry());
			newOffices.setTerritory(pOffices.getTerritory());

			COffices savenewOffices = iOfficesRepository.save(newOffices);
			return new ResponseEntity<>(savenewOffices, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create New Offices: " + e.getCause().getCause().getMessage());
		}
	}

	// Update Offices Exist
	@CrossOrigin
	@PutMapping("/Offices/{OfficesId}")
	public ResponseEntity<Object> updateOffices( @PathVariable("OfficesId") Long OfficesId,
			@RequestBody COffices pOffices) {
		Optional<COffices> existOffices = iOfficesRepository.findById(OfficesId);
		if (existOffices.isPresent()) {
			COffices updateOffices = existOffices.get();
			updateOffices.setCity(pOffices.getCity());
			updateOffices.setPhone(pOffices.getPhone());
			updateOffices.setAddressLine(pOffices.getAddressLine());
			updateOffices.setState(pOffices.getState());
			updateOffices.setCountry(pOffices.getCountry());
			updateOffices.setTerritory(pOffices.getTerritory());

			COffices saveUpdateOffices = iOfficesRepository.save(updateOffices);
			return new ResponseEntity<>(saveUpdateOffices, HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Khong ton tai Offices de update", HttpStatus.NOT_FOUND);
		}
	}

}
