package com.devcamp.shop24h.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.shop24h.model.CCustomer;
import com.devcamp.shop24h.model.CPayment;
import com.devcamp.shop24h.repository.ICustomerRepository;
import com.devcamp.shop24h.repository.IPaymentRepository;
import com.devcamp.shop24h.service.ExcelExporterPaymentByDay;
import com.devcamp.shop24h.service.ExcelExporterPaymentByMonth;
import com.devcamp.shop24h.service.ExcelExporterPaymentByWeek;
import com.devcamp.shop24h.serviceRepo.IPaymentCountByDay;
import com.devcamp.shop24h.serviceRepo.ITotalPaymentByCustomer;

@RestController
public class CPaymentController {

	@Autowired
	private IPaymentRepository iPaymentRepository;
	@Autowired
	private ICustomerRepository iCustomerRepository;
	
	// Get all payments information
	@CrossOrigin
	@GetMapping("/payments")
	public ResponseEntity<List<CPayment>> getAllpayments() {
		try {
			List<CPayment> listAllPayments = new ArrayList<CPayment>();
			iPaymentRepository.findAll().forEach(listAllPayments::add);
			return new ResponseEntity<>(listAllPayments, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// Get info payments by Id
	@CrossOrigin
	@GetMapping("/payments/{paymentId}")
	public CPayment getpaymentsInfoById(@PathVariable Long paymentId) {
		if (iPaymentRepository.findById(paymentId).isPresent())
			return iPaymentRepository.findById(paymentId).get();
		else
			return null;
	}

	// Delete payments by Id
	@CrossOrigin
	@DeleteMapping("/payments/{paymentId}")
	public ResponseEntity<Object> deletePaymentById(@PathVariable Long paymentId) {
		try {
			iPaymentRepository.deleteById(paymentId);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// Delete All payments
	@CrossOrigin
	@DeleteMapping("/payments")
	public ResponseEntity<CPayment> deleteAllPayments() {
		try {
			iPaymentRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	//Code Create new chua chay 
	// Create new payments
	@CrossOrigin
	@PostMapping("/customer/{customerId}/payments")
	public ResponseEntity<Object> createNewpayments(@PathVariable("customerId") Long customerId,
			@RequestBody CPayment pPayment) {
		try {
			
			Optional<CCustomer> customerData = iCustomerRepository.findById(customerId);
			if (customerData.isPresent()) {
				CPayment newPayment = new CPayment();
				newPayment.setAmmount(pPayment.getAmmount());
				newPayment.setCheckNumber(pPayment.getCheckNumber());
				newPayment.setPaymentDate(pPayment.getPaymentDate());
				//newPayment.setCustomer(pPayment.getCustomer());

				CCustomer customer = customerData.get();
				newPayment.setCustomer(customer);
				//newPayment.setPhoneNumber(customer.getPhoneNumber());
				
				CPayment saveNewPayment = iPaymentRepository.save(newPayment);
				return new ResponseEntity<>(saveNewPayment, HttpStatus.CREATED);
			}else {
				return new ResponseEntity<>("Customer khong ton tai", HttpStatus.NOT_FOUND);
			}
			
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create New payments: " + e.getCause().getCause().getMessage());
		}
	}

	// Update payments Exist
	@CrossOrigin
	@PutMapping("/customer/{customerId}/payments/{paymentId}")
	public ResponseEntity<Object> updatePayment( @PathVariable("customerId") Long customerId,
			@PathVariable("paymentId") Long paymentId, @RequestBody CPayment pPayment) {
		Optional<CCustomer> existCustomer = iCustomerRepository.findById(customerId);
		Optional<CPayment> existPayment = iPaymentRepository.findById(paymentId);
		if (existCustomer.isPresent() & existPayment.isPresent()) {
			CPayment updatePayment = existPayment.get();
			updatePayment.setAmmount(pPayment.getAmmount());
			updatePayment.setCheckNumber(pPayment.getCheckNumber());
			updatePayment.setPaymentDate(pPayment.getPaymentDate());
			//updatePayment.setCustomer(pPayment.getCustomer());
			
			CPayment saveUpdatepayments = iPaymentRepository.save(updatePayment);
			return new ResponseEntity<>(saveUpdatepayments, HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Khong ton tai payments de update", HttpStatus.NOT_FOUND);
		}
	}
	
	//--------------------SHOP24h-----------------------------
	//Querry Thuc hien xem total payment tung ngay, theo thu tu ngay giam dan
	@CrossOrigin
	@GetMapping("/paymentByDay")
	public ResponseEntity<List<IPaymentCountByDay>> getTotalPaymentByDay() {
		try {
			List<IPaymentCountByDay> listAllPayments = new ArrayList<IPaymentCountByDay>();
			iPaymentRepository.findPaymentTotalLastDay().forEach(listAllPayments::add);
			return new ResponseEntity<>(listAllPayments, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	//Querry Thuc hien xem total payment tung tuan trong nam, theo thu tu tuan giam dan
	@CrossOrigin
	@GetMapping("/paymentByWeek")
	public ResponseEntity<List<IPaymentCountByDay>> getTotalPaymentByWeek() {
		try {
			List<IPaymentCountByDay> listAllPayments = new ArrayList<IPaymentCountByDay>();
			iPaymentRepository.findPaymentTotalByWeek().forEach(listAllPayments::add);
			return new ResponseEntity<>(listAllPayments, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	//Querry Thuc hien xem total payment tung thang trong nam, theo thu tu thang giam dan
	@CrossOrigin
	@GetMapping("/paymentByMonth")
	public ResponseEntity<List<IPaymentCountByDay>> getTotalPaymentByMonth() {
		try {
			List<IPaymentCountByDay> listAllPayments = new ArrayList<IPaymentCountByDay>();
			iPaymentRepository.findPaymentTotalByMonth().forEach(listAllPayments::add);
			return new ResponseEntity<>(listAllPayments, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	//-------------shop24h excel Export---------------
	//Eport by Day
	@GetMapping("/export/totalAmountByDay/excel")
	public void exportToExcel(HttpServletResponse response) throws IOException {
		response.setContentType("application/octet-stream");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());

		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=payment_by_day_" + currentDateTime + ".xlsx";
		response.setHeader(headerKey, headerValue);

		List<IPaymentCountByDay> listPaymentByDay = new ArrayList<IPaymentCountByDay>();

		iPaymentRepository.findPaymentTotalLastDay().forEach(listPaymentByDay::add);

		ExcelExporterPaymentByDay excelExporter = new ExcelExporterPaymentByDay(listPaymentByDay);

		excelExporter.export(response);
	}
	//Eport by Week
	@GetMapping("/export/totalAmountByWeek/excel")
	public void exportToExcelByWeek(HttpServletResponse response) throws IOException {
		response.setContentType("application/octet-stream");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());
		
		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=payment_by_week_" + currentDateTime + ".xlsx";
		response.setHeader(headerKey, headerValue);
		
		List<IPaymentCountByDay> listPaymentByWeek = new ArrayList<IPaymentCountByDay>();
		
		iPaymentRepository.findPaymentTotalByWeek().forEach(listPaymentByWeek::add);
		
		ExcelExporterPaymentByWeek excelExporter = new ExcelExporterPaymentByWeek(listPaymentByWeek);
		
		excelExporter.export(response);
	}
	//Eport by Month
	@GetMapping("/export/totalAmountByMonth/excel")
	public void exportToExcelByMonth(HttpServletResponse response) throws IOException {
		response.setContentType("application/octet-stream");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());
		
		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=payment_by_month_" + currentDateTime + ".xlsx";
		response.setHeader(headerKey, headerValue);
		
		List<IPaymentCountByDay> listPaymentByMonth = new ArrayList<IPaymentCountByDay>();
		
		iPaymentRepository.findPaymentTotalByMonth().forEach(listPaymentByMonth::add);
		
		ExcelExporterPaymentByMonth excelExporter = new ExcelExporterPaymentByMonth(listPaymentByMonth);
		
		excelExporter.export(response);
	}
	
	//------------report by customer-----------------
	//Querry Thuc hien xem total payment tung thang trong nam, theo thu tu thang giam dan
	@CrossOrigin
	@GetMapping("/payment/{value1}/{value2}")
	public ResponseEntity<List<ITotalPaymentByCustomer>> getTotalPaymentByCustomer(@PathVariable("value1") int value1, @PathVariable("value2") int value2) {
		try {
			List<ITotalPaymentByCustomer> listAllPayments = new ArrayList<ITotalPaymentByCustomer>();
			iPaymentRepository.findTotalPaymentByCustomer(value1, value2).forEach(listAllPayments::add);
			return new ResponseEntity<>(listAllPayments, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
