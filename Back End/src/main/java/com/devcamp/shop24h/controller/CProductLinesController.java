package com.devcamp.shop24h.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.shop24h.model.CProductLines;
import com.devcamp.shop24h.repository.IProductLineRepository;

@RestController
public class CProductLinesController {

	@Autowired
	private IProductLineRepository iProductLineRepository;

	// Get all ProductLines information
	@CrossOrigin
	@GetMapping("/ProductLines")
	public ResponseEntity<List<CProductLines>> getAllProductLines() {
		try {
			List<CProductLines> listAllProductLines = new ArrayList<CProductLines>();
			iProductLineRepository.findAll().forEach(listAllProductLines::add);
			return new ResponseEntity<>(listAllProductLines, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// Get info ProductLines by Id
	@CrossOrigin
	@GetMapping("/ProductLines/{ProductLinesId}")
	public CProductLines getProductLinesInfoById(@PathVariable Long ProductLinesId) {
		if (iProductLineRepository.findById(ProductLinesId).isPresent())
			return iProductLineRepository.findById(ProductLinesId).get();
		else
			return null;
	}

	// Delete ProductLines by Id
	@CrossOrigin
	@DeleteMapping("/ProductLines/{ProductLinesId}")
	public ResponseEntity<Object> deleteProductLinesById(@PathVariable Long ProductLinesId) {
		try {
			iProductLineRepository.deleteById(ProductLinesId);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// Delete All ProductLines
	@CrossOrigin
	@DeleteMapping("/ProductLines")
	public ResponseEntity<CProductLines> deleteAllProductLines() {
		try {
			iProductLineRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// Create new ProductLines
	@CrossOrigin
	@PostMapping("/ProductLines")
	public ResponseEntity<Object> createNewProductLines( @RequestBody CProductLines pProductLines) {
		try {
			CProductLines newProductLines = new CProductLines();
			newProductLines.setDescription(pProductLines.getDescription());
			newProductLines.setProductLine(pProductLines.getProductLine());
			newProductLines.setcProducs(pProductLines.getcProducs());

			CProductLines savenewProductLines = iProductLineRepository.save(newProductLines);
			return new ResponseEntity<>(savenewProductLines, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create New ProductLines: " + e.getCause().getCause().getMessage());
		}
	}

	// Update ProductLines Exist
	@CrossOrigin
	@PutMapping("/ProductLines/{ProductLinesId}")
	public ResponseEntity<Object> updateProductLines( @PathVariable("ProductLinesId") Long ProductLinesId,
			@RequestBody CProductLines pProductLines) {
		Optional<CProductLines> existProductLines = iProductLineRepository.findById(ProductLinesId);
		if (existProductLines.isPresent()) {
			CProductLines updateProductLines = existProductLines.get();
			updateProductLines.setDescription(pProductLines.getDescription());
			updateProductLines.setProductLine(pProductLines.getProductLine());
			updateProductLines.setcProducs(pProductLines.getcProducs());
			
			CProductLines saveUpdateProductLines = iProductLineRepository.save(updateProductLines);
			return new ResponseEntity<>(saveUpdateProductLines, HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Khong ton tai ProductLines de update", HttpStatus.NOT_FOUND);
		}
	}

}
