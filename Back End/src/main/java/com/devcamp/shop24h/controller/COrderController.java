package com.devcamp.shop24h.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.shop24h.model.CCustomer;
import com.devcamp.shop24h.model.COrder;
import com.devcamp.shop24h.model.COrderDetail;
import com.devcamp.shop24h.repository.ICustomerRepository;
import com.devcamp.shop24h.repository.IOrderDetailRepository;
import com.devcamp.shop24h.repository.IOrderRepository;

@RestController
public class COrderController {

	@Autowired
	private IOrderRepository iOrderRepository;
	@Autowired
	private ICustomerRepository iCustomerRepository;
	@Autowired
	private IOrderDetailRepository iOrderDetailRepository;
	// Get all Orders information
	@CrossOrigin
	@GetMapping("/Orders")
	public ResponseEntity<List<COrder>> getAllOrders() {
		try {
			List<COrder> listAllOrders = new ArrayList<COrder>();
			iOrderRepository.findAll().forEach(listAllOrders::add);
			return new ResponseEntity<>(listAllOrders, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@CrossOrigin
	@GetMapping("/OrdersASC")
	public ResponseEntity<List<COrder>> getAllOrdersASC() {
		try {
			List<COrder> listAllOrders = new ArrayList<COrder>();
			iOrderRepository.getOrderASC().forEach(listAllOrders::add);
			return new ResponseEntity<>(listAllOrders, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// Get info Orders by Id
	@CrossOrigin
	@GetMapping("/Orders/{orderId}")
	public COrder getOrderDetailInfoById(@PathVariable Long orderId) {
		if (iOrderRepository.findById(orderId).isPresent())
			return iOrderRepository.findById(orderId).get();
		else
			return null;
	}
	// Get info Orders by Id
	@CrossOrigin
	@GetMapping("/Orders/{orderId}/OrderDetail")
	public ResponseEntity<Object>  getListOrderDetailById(@PathVariable Long orderId) {
		Optional<COrder> existOrder = iOrderRepository.findById(orderId);
		if (existOrder.isPresent()) {
			
			List<COrderDetail> OrderDetail = existOrder.get().getcOrderDetails();
		
			List<COrderDetail> saveOrderDetail = iOrderDetailRepository.saveAll(OrderDetail);
			
			return new ResponseEntity<>(saveOrderDetail, HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Khong ton tai Order", HttpStatus.NOT_FOUND);
		}
	}

	// Delete Orders by Id
	@CrossOrigin
	@DeleteMapping("/Orders/{orderId}")
	public ResponseEntity<Object> deleteOrderById(@PathVariable Long orderId) {
		try {
			iOrderRepository.deleteById(orderId);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// Delete All Orders
	@CrossOrigin
	@DeleteMapping("/Orders")
	public ResponseEntity<COrder> deleteAllOrders() {
		try {
			iOrderRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	
	// Create new Orders
	@CrossOrigin
	@PostMapping("/customers/{customerId}/Order")
	public ResponseEntity<Object> createNewOrder(@PathVariable("customerId") Long customerId,
			@RequestBody COrder pOrder) {
		try {
			
			Optional<CCustomer> customerData = iCustomerRepository.findById(customerId);
			if (customerData.isPresent()) {
				COrder newOrder = new COrder();
				newOrder.setOrderDate(pOrder.getOrderDate());
				newOrder.setRequiredDate(pOrder.getRequiredDate());
				newOrder.setShippedDate(pOrder.getShippedDate());
				newOrder.setStatus(pOrder.getStatus());
				newOrder.setComments(pOrder.getComments());
				//newOrder.setcustomers(pOrder.getcustomers());

				CCustomer customers = customerData.get();
				newOrder.setCustomer(customers);
				
				COrder saveNewOrder = iOrderRepository.save(newOrder);
				return new ResponseEntity<>(saveNewOrder, HttpStatus.CREATED);
			}else {
				return new ResponseEntity<>("customers khong ton tai", HttpStatus.NOT_FOUND);
			}
			
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create New Orders: " + e.getCause().getCause().getMessage());
		}
	}

	// Update Orders Exist
	@CrossOrigin
	@PutMapping("/customers/{customerId}/Orders/{orderId}")
	public ResponseEntity<Object> updateOrderDetail( @PathVariable("customerId") Long customerId,
			@PathVariable("orderId") Long orderId, @RequestBody COrder pOrder) {
		
		Optional<CCustomer> existCustomer = iCustomerRepository.findById(customerId);
		Optional<COrder> existOrder = iOrderRepository.findById(orderId);
		
		if (existCustomer.isPresent() & existOrder.isPresent()) {
			COrder updateOrder = existOrder.get();
			updateOrder.setOrderDate(pOrder.getOrderDate());
			updateOrder.setRequiredDate(pOrder.getRequiredDate());
			updateOrder.setShippedDate(pOrder.getShippedDate());
			updateOrder.setStatus(pOrder.getStatus());
			updateOrder.setComments(pOrder.getComments());
			//updateOrderDetail.setcustomers(pOrder.getcustomers());
			
			COrder saveUpdateOrder = iOrderRepository.save(updateOrder);
			return new ResponseEntity<>(saveUpdateOrder, HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Khong ton tai Orders de update", HttpStatus.NOT_FOUND);
		}
	}
	
	

}
