package com.devcamp.shop24h.controller;

import java.util.Optional;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.shop24h.model.*;
import com.devcamp.shop24h.repository.*;

@CrossOrigin
@RestController
public class CCommentCotroller {

	@Autowired
	IProductRepository iProductRepo;
	@Autowired
	ICommentRepository iCommentRepo;
	@Autowired
	UserRepository iUserRepo;
//	tạo mới comment

	// Get all comment
	@CrossOrigin
	@GetMapping("/comments")
	public ResponseEntity<List<CComment>> getAllComments() {
		try {
			List<CComment> listAllComments = new ArrayList<CComment>();
			iCommentRepo.findAll().forEach(listAllComments::add);
			return new ResponseEntity<>(listAllComments, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// Get all comment cua san pham co product id
	@CrossOrigin
	@GetMapping("/comments/{productId}")
	public ResponseEntity<List<CComment>> getAllCommentsByroductId(@PathVariable long productId) {
		try {

			List<CComment> listAllCommentsByProductId = new ArrayList<CComment>();
			iCommentRepo.findAllCommentsByProductId(productId).forEach(listAllCommentsByProductId::add);
			return new ResponseEntity<>(listAllCommentsByProductId, HttpStatus.OK);

		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@PostMapping("/users/{userId}/products/{productId}/comments")
	public ResponseEntity<Object> createNewComments(@PathVariable long productId, @PathVariable long userId,
			@RequestBody CComment comment) {
		try {
			Optional<CProducts> existProduct = iProductRepo.findById(productId);
			Optional<User> existUser = iUserRepo.findById(userId);

			if (existProduct.isPresent() && existUser.isPresent()) {
				CComment newComment = new CComment();
				newComment.setComments(comment.getComments());
				newComment.setRateStar(comment.getRateStar());

				CProducts _product = existProduct.get();
				newComment.setProduct(_product);
				User _user = existUser.get();
				newComment.setUser(_user);

				//newComment.setName(_user.getFirstName() + " " + _user.getLastName());
				newComment.setName(_user.getUserName());

				CComment saveComment = iCommentRepo.save(newComment);
				return new ResponseEntity<>(saveComment, HttpStatus.CREATED);
			} else {
				return new ResponseEntity<>("product or user khong ton tai", HttpStatus.NOT_FOUND);
			}

		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

//	tính rating trung bình của comment
	@GetMapping("/products/{productId}/average")
	public ResponseEntity<Object> getAverageRate(@PathVariable long productId) {
		try {
			Optional<CProducts> existProduct = iProductRepo.findById(productId);
			if (existProduct.isPresent()) {				
				return new ResponseEntity<>(iCommentRepo.getAvgRatingProductId(productId) ,HttpStatus.OK);
			} else {
				return new ResponseEntity<>("chua co danh gia",HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	

//	
//	@PutMapping("/comments/{commentId}")
//	public ResponseEntity<Object> editComments(@PathVariable int commentId, @Valid Comments newComments) {
//		try {
//			Optional<Comments> foundComments = commentsRepo.findById(commentId);
//			if (foundComments.isPresent()) {	
//				Comments updateComments = foundComments.get();
//				updateComments.setComments(newComments.getComments());
//				updateComments.setRateStar(newComments.getRateStar());
//				return new ResponseEntity<>(commentsRepo.save(updateComments) ,HttpStatus.OK);
//			} else {
//				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//			}
//		} catch (Exception e) {
//			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
//		}
//	}
//	
////	xóa tất cả comment
//	@DeleteMapping("/comments")
//	public ResponseEntity<Object> deleteAllCustomer() {
//		try {
//			commentsRepo.deleteAll();
//			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
//		} catch (Exception e) {
//			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
//		}
//	}
//
////	xóa đánh giá dựa trên id
//	@DeleteMapping("/comments/{commentId}")
//	public ResponseEntity<Object> deleteCustomerById(@PathVariable int commentId) {
//		try {
//			Optional<Comments> commentFound = commentsRepo.findById(commentId);
//			if (commentFound.isPresent()) {
//				commentsRepo.deleteById(commentId);
//				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
//			} else {
//				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//			}
//		} catch (Exception e) {
//			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
//		}
//	}
}
