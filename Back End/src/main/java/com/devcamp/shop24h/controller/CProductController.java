package com.devcamp.shop24h.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.shop24h.model.CProductLines;
import com.devcamp.shop24h.model.CProducts;
import com.devcamp.shop24h.repository.IProductLineRepository;
import com.devcamp.shop24h.repository.IProductRepository;

@RestController
public class CProductController {

	@Autowired
	private IProductRepository iProductRepository;

	@Autowired
	private IProductLineRepository iProductLineRepository;

	// Get all product information
	@CrossOrigin
	@GetMapping("/products")
	public ResponseEntity<List<CProducts>> getAllProducts() {
		try {
			List<CProducts> listAllProducts = new ArrayList<CProducts>();
			iProductRepository.findAll().forEach(listAllProducts::add);
			return new ResponseEntity<>(listAllProducts, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// Get info Product by Id
	@CrossOrigin
	@GetMapping("/products/{productsId}")
	public CProducts getProductInfoByproductsId(@PathVariable Long productsId) {
		if (iProductRepository.findById(productsId).isPresent())
			return iProductRepository.findById(productsId).get();
		else
			return null;
	}

	// Delete Product by Id
	@CrossOrigin
	@DeleteMapping("/products/{productsId}")
	public ResponseEntity<Object> deleteProductById(@PathVariable Long productsId) {
		try {
			iProductRepository.deleteById(productsId);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// Delete All Product
	@CrossOrigin
	@DeleteMapping("/products")
	public ResponseEntity<CProducts> deleteAllProduct() {
		try {
			iProductRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// Create new Product
	@CrossOrigin
	@PostMapping("/productLines/{productineId}/product")
	public ResponseEntity<Object> createNewProduct(@PathVariable("productineId") Long productineId,
			@RequestBody CProducts pProduct) {
		try {

			Optional<CProductLines> productLineData = iProductLineRepository.findById(productineId);
			if (productLineData.isPresent()) {
				CProducts newProduct = new CProducts();
				newProduct.setBuyPrice(pProduct.getBuyPrice());
				newProduct.setProductCode(pProduct.getProductCode());
				newProduct.setProductDescription(pProduct.getProductDescription());
				newProduct.setProductImage(pProduct.getProductImage());
				newProduct.setProductName(pProduct.getProductName());
				newProduct.setQuantityInStock(pProduct.getQuantityInStock());
				newProduct.setSellPrice(pProduct.getSellPrice());

				CProductLines _ProductLine = productLineData.get();
				newProduct.setProductLine(_ProductLine);

				CProducts saveNewProduct = iProductRepository.save(newProduct);
				return new ResponseEntity<>(saveNewProduct, HttpStatus.CREATED);
			} else {
				return new ResponseEntity<>("product line khong ton tai", HttpStatus.NOT_FOUND);
			}

		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create New Product: " + e.getCause().getCause().getMessage());
		}
	}

	// Code update chua chay
	// Update Product Exist
	@CrossOrigin
	@PutMapping("/productLines/{productineId}/product/{productId}")
	public ResponseEntity<Object> updateProduct(@PathVariable("productineId") Long productineId,
			@PathVariable("productId") Long productId, @RequestBody CProducts pProduct) {

		Optional<CProductLines> existProductLine = iProductLineRepository.findById(productineId);
		Optional<CProducts> existProduct = iProductRepository.findById(productId);
		if (existProductLine.isPresent() & existProduct.isPresent()) {

			CProducts updateProduct = existProduct.get();
			updateProduct.setProductName(pProduct.getProductName());
			updateProduct.setProductDescription(pProduct.getProductDescription());
			updateProduct.setProductImage(pProduct.getProductImage());
			updateProduct.setQuantityInStock(pProduct.getQuantityInStock());
			updateProduct.setBuyPrice(pProduct.getBuyPrice());
			updateProduct.setProductCode(pProduct.getProductCode());
			updateProduct.setSellPrice(pProduct.getSellPrice());

			CProducts saveUpdateProduct = iProductRepository.save(updateProduct);
			return new ResponseEntity<>(saveUpdateProduct, HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Khong ton tai Product or productLine de update", HttpStatus.NOT_FOUND);
		}
	}

	// Get 6 product to homePage
	@CrossOrigin
	@GetMapping("/homePageProducts")
	public ResponseEntity<List<CProducts>> getHomePageProducts() {
		try {
			List<CProducts> listAllProducts = new ArrayList<CProducts>();
			iProductRepository.listProductHomePage().forEach(listAllProducts::add);
			return new ResponseEntity<>(listAllProducts, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// get product vao trang all product, phan trang 9 san pham tren 1 trang
	@CrossOrigin
	@GetMapping("/listProducts/page/{page}")
	public ResponseEntity<List<CProducts>> getAllCustomersByCountry(@PathVariable("page") int page) {
		try {
			List<CProducts> listAllProducts = new ArrayList<CProducts>();
			iProductRepository.findProducts(PageRequest.of(page, 9)).forEach(listAllProducts::add);
			return new ResponseEntity<>(listAllProducts, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	//COntroller thuc hien chuc nang filter san pham theo dieu kien nhap vao
	@CrossOrigin
	@GetMapping("/listProducts/filter/{value}/{price1}/{price2}/{page}")
	public ResponseEntity<List<CProducts>> getFilter(@PathVariable("value") int value,
			@PathVariable("price1") int price1,@PathVariable("price2") int price2, @PathVariable("page") int page) {
		try {
			List<CProducts> listAllProductsFilter = new ArrayList<CProducts>();
			iProductRepository.findFilter(value, price1,price2,  PageRequest.of(page, 9)).forEach(listAllProductsFilter::add);
			return new ResponseEntity<>(listAllProductsFilter, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


}
