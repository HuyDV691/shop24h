package com.devcamp.shop24h.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.shop24h.model.CEmployees;
import com.devcamp.shop24h.repository.IEmployeesRepository;

@RestController
public class CEmployeesController {

	@Autowired
	private IEmployeesRepository iEmployeesRepository;

	// Get all employees information
	@CrossOrigin
	@GetMapping("/employees")
	public ResponseEntity<List<CEmployees>> getAllEmployees() {
		try {
			List<CEmployees> listAllemployees = new ArrayList<CEmployees>();
			iEmployeesRepository.findAll().forEach(listAllemployees::add);
			return new ResponseEntity<>(listAllemployees, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// Get info employees by Id
	@CrossOrigin
	@GetMapping("/employees/{employeesId}")
	public CEmployees getEmployeesInfoById(@PathVariable Long employeesId) {
		if (iEmployeesRepository.findById(employeesId).isPresent())
			return iEmployeesRepository.findById(employeesId).get();
		else
			return null;
	}

	// Delete employees by Id
	@CrossOrigin
	@DeleteMapping("/employees/{employeesId}")
	public ResponseEntity<Object> deleteemployeesById(@PathVariable Long employeesId) {
		try {
			iEmployeesRepository.deleteById(employeesId);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// Delete All employees
	@CrossOrigin
	@DeleteMapping("/employees")
	public ResponseEntity<CEmployees> deleteAllemployees() {
		try {
			iEmployeesRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// Create new employees
	@CrossOrigin
	@PostMapping("/employees")
	public ResponseEntity<Object> createnewEmployees( @RequestBody CEmployees pEmployees) {
		try {
			CEmployees newEmployees = new CEmployees();
			newEmployees.setFirstName(pEmployees.getFirstName());
			newEmployees.setLastName(pEmployees.getLastName());
			newEmployees.setExtension(pEmployees.getExtension());
			newEmployees.setEmail(pEmployees.getEmail());
			newEmployees.setOfficeCode(pEmployees.getOfficeCode());
			newEmployees.setReportTo(pEmployees.getReportTo());
			newEmployees.setJobTitle(pEmployees.getJobTitle());

			CEmployees savenewEmployees = iEmployeesRepository.save(newEmployees);
			return new ResponseEntity<>(savenewEmployees, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create New employees: " + e.getCause().getCause().getMessage());
		}
	}

	// Update employees Exist
	@CrossOrigin
	@PutMapping("/employees/{employeesId}")
	public ResponseEntity<Object> updateemployees( @PathVariable("employeesId") Long employeesId,
			@RequestBody CEmployees pEmployees) {
		Optional<CEmployees> existEmployees = iEmployeesRepository.findById(employeesId);
		if (existEmployees.isPresent()) {
			CEmployees updateEmployees = existEmployees.get();
			updateEmployees.setFirstName(pEmployees.getFirstName());
			updateEmployees.setLastName(pEmployees.getLastName());
			updateEmployees.setExtension(pEmployees.getExtension());
			updateEmployees.setEmail(pEmployees.getEmail());
			updateEmployees.setOfficeCode(pEmployees.getOfficeCode());
			updateEmployees.setReportTo(pEmployees.getReportTo());
			updateEmployees.setJobTitle(pEmployees.getJobTitle());

			CEmployees saveUpdateemployees = iEmployeesRepository.save(updateEmployees);
			return new ResponseEntity<>(saveUpdateemployees, HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Khong ton tai employees de update", HttpStatus.NOT_FOUND);
		}
	}

}
