package com.devcamp.shop24h.serviceRepo;


public interface IPaymentCountByDay {
	
	public String getDayName();
	public int getTotalAmount();
	
	public String getMonthNumber();
	public String getWeekNumber();
}
