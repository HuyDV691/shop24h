package com.devcamp.shop24h.serviceRepo;

public interface ICountAvgRating {
	public int getAvgRating();
}
