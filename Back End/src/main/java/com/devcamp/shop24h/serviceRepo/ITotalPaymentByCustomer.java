package com.devcamp.shop24h.serviceRepo;


public interface ITotalPaymentByCustomer {
	
	public String getFirst_Name();
	public String getLast_Name();
	public int getTotalAmount();

}
