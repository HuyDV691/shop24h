package com.devcamp.shop24h.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shop24h.model.CEmployees;

public interface IEmployeesRepository extends JpaRepository<CEmployees, Long> {

}
