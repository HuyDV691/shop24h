package com.devcamp.shop24h.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shop24h.model.COffices;

public interface IOfficesRepository extends JpaRepository<COffices, Long> {

}
