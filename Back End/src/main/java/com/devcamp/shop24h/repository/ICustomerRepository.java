package com.devcamp.shop24h.repository;

import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.devcamp.shop24h.model.CCustomer;

public interface ICustomerRepository extends JpaRepository<CCustomer, Long> {
	CCustomer findBycOrders(Long id);

	CCustomer findByUserId(Long userId);

	// Task 79.60: Viết query cho bảng customers cho phép tìm danh sách
	// theo họ hoặc tên với LIKE
	@Query(value = "SELECT * FROM customers c WHERE c.last_name LIKE :inputName OR c.first_name LIKE :inputName", nativeQuery = true)
	List<CCustomer> findCustomerByName(@Param("inputName") String inputName);

	// Task 79.60:Viết query cho bảng customers cho phép tìm danh sách
	// theo city, state với LIKE có phân trang.

	@Query(value = "SELECT * FROM customers c WHERE c.city  LIKE :inputData OR c.state  LIKE :inputData ", nativeQuery = true)
	List<CCustomer> findCustomerByCity(@Param("inputData") String inputData, Pageable pageable);

	// Task 79.60:Viết query cho bảng customers cho phép tìm danh sách
	// theo country có phân trang và ORDER BY tên.
	@Query(value = "SELECT * FROM customers c WHERE c.country LIKE :country ORDER BY c.first_name ASC", nativeQuery = true)
	List<CCustomer> findCustomerByCountry(@Param("country") String country, Pageable pageable);

	// Task 79.60:Viết query cho bảng customers cho phép UPDATE dữ liệu có country =
	// NULL
	// với giá trị truyền vào từ tham số

	@Transactional
	@Modifying
	@Query(value = "UPDATE customers c SET c.country = :updateCountry WHERE c.country = 'null'", nativeQuery = true)
	int updateCustomerCountry(@Param("updateCountry") String updateCountry);

	// UPDATE customers c SET c.state = "Test" WHERE c.state = "null"

	// ------------------------Shop24h-------------------
	// Viết query tim phone number customer trong database, ket qua tra ve la id cua
	// customer do
	@Query(value = "SELECT * FROM customers c WHERE c.phone_number LIKE :inpPhoneNumber", nativeQuery = true)
	List<CCustomer> findCustomerByPhoneNumber(@Param("inpPhoneNumber") String inpPhoneNumber);

	// Viết query lay du lieu customer theo userId
	@Query(value = "SELECT * FROM customers c WHERE c.user_id = :userId", nativeQuery = true)
	CCustomer findCustomerByUserId(@Param("userId") Long userId);

	
}
