package com.devcamp.shop24h.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.shop24h.model.CComment;
import com.devcamp.shop24h.serviceRepo.ICountAvgRating;

public interface ICommentRepository extends JpaRepository<CComment, Integer> {
	
	//Query thuc hien chuc nang lay toan bo comment cua san pham co product id bat ky
	@Query(value = "SELECT * FROM comments c WHERE c.product_id = :productId ORDER BY c.id DESC", nativeQuery = true)
	List<CComment> findAllCommentsByProductId(@Param("productId") long productId);

	//Query thuc hien chuc nang lay gia tri trung binh rate star cua san phan co product Id
	//SELECT c.product_id , AVG(c.rate_star) AS avgRating FROM comments c WHERE c.product_id = 6
	@Query(value = "SELECT c.product_id , AVG(c.rate_star) AS avgRating FROM comments c WHERE c.product_id = :productId", nativeQuery = true)
	ICountAvgRating getAvgRatingProductId(@Param("productId") long productId);
}
