package com.devcamp.shop24h.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shop24h.model.COrderDetail;

public interface IOrderDetailRepository extends JpaRepository<COrderDetail, Long> {

}
