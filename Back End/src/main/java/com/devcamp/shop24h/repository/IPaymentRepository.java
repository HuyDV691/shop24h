package com.devcamp.shop24h.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.shop24h.model.CPayment;
import com.devcamp.shop24h.serviceRepo.IPaymentCountByDay;
import com.devcamp.shop24h.serviceRepo.ITotalPaymentByCustomer;

public interface IPaymentRepository extends JpaRepository<CPayment, Long> {

	// Xem total payment theo tung ngay
	@Query(value = "SELECT DAYNAME(payment_date) as dayName, SUM(ammount) as totalAmount "
			+ "FROM payments  GROUP BY payment_date ORDER BY `payments`.`payment_date` DESC", nativeQuery = true)
	List<IPaymentCountByDay> findPaymentTotalLastDay();

	// Xem payment Total theo week by year
	@Query(value = "SELECT DATE_FORMAT(payment_date, '%Y-%U') AS weekNumber, SUM(ammount) as totalAmount "
			+ "FROM payments GROUP BY DATE_FORMAT(payment_date, '%Y-%U') ORDER BY `weekNumber` DESC", nativeQuery = true)
	List<IPaymentCountByDay> findPaymentTotalByWeek();

	// Xem payment Total theo month by year
	@Query(value = "SELECT *, DATE_FORMAT(payment_date, '%Y-%M') AS monthNumber, SUM(ammount) as totalAmount "
			+ "FROM payments GROUP BY DATE_FORMAT(payment_date, '%Y-%M') ORDER BY `monthNumber` DESC", nativeQuery = true)
	List<IPaymentCountByDay> findPaymentTotalByMonth();

	// --------Phan loai khach hang theo request param truyen vao------------------

	@Query(value = "SELECT * FROM(SELECT c.first_name, c.last_name, SUM(p.ammount) as totalAmount FROM payments p "
			+ "JOIN customers c ON p.customer_id = c.id GROUP BY p.customer_id) as new"
			+ " WHERE new.totalAmount BETWEEN :value1 AND :value2", nativeQuery = true)
	List<ITotalPaymentByCustomer> findTotalPaymentByCustomer(@Param("value1") int value1, @Param("value2") int value2);
}
