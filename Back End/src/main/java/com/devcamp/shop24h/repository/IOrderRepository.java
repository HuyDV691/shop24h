package com.devcamp.shop24h.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.devcamp.shop24h.model.COrder;

public interface IOrderRepository extends JpaRepository<COrder, Long> {
	@Query(value = "SELECT * FROM orders ORDER BY customer_id ASC", nativeQuery = true)
	List<COrder> getOrderASC();

}
