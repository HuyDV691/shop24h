package com.devcamp.shop24h.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.shop24h.model.CProducts;

public interface IProductRepository extends JpaRepository<CProducts, Long> {

	//Query cho phop hien thi 6 product ra trang chu
	@Query(value = "SELECT * FROM products p LIMIT 6", nativeQuery = true)
	List<CProducts> listProductHomePage();
	
	//query co chuc nang phan trang cho product
	@Query(value = "SELECT * FROM products p", nativeQuery = true)
	List<CProducts> findProducts(Pageable pageable);
	
	//Query co chuc nang filter san pham
	@Query(value = "SELECT * FROM products p "
			+ "WHERE (p.product_line_id = :value) "
			+ "AND (p.sell_price BETWEEN :price1 AND :price2)"
			, nativeQuery = true)
	List<CProducts> findFilter(@Param("value") int value,
			@Param("price1") int price1, @Param("price2") int price2, Pageable pageable);
	
}
