package com.devcamp.shop24h.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shop24h.model.CProductLines;

public interface IProductLineRepository extends JpaRepository<CProductLines, Long> {

}
